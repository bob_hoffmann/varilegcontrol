#include <cstdio>
#include <cstdlib>
#include <vector>
#include "spline.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>

/*
compile && plot:
$           g++ foot_spline.cpp -o foot_spline &&  ./foot_spline && gnuplot
gnuplot>    set multiplot layout 2,1 rowsfirst; plot "xy_spline.dat" with lines, "xy.dat", "xy6.dat", "xy6_spline.dat" with lines,"xy7.dat", "xy7_spline.dat" with lines; plot "phi_knee.dat" with lines,"phi_hip.dat" with lines, "phi_knee_6.dat" with lines, "phi_hip_6.dat" with lines, "phi_knee_7.dat" with lines, "phi_hip_7.dat" with lines



*/

#define FS 1
#define LS 1

using namespace std;


int main(int argc, char** argv) {

    const double step_length=				0.5;// [m]
    const double step_height_begin=			0.23; // [m]
    const double step_height_end=			0.08; // [m]
    const double T_Gait= 					6.00; // [s]
    const double perc_time_on_floor=		0.5; // [-]	

    const int N_points = 19;

    tk::spline sx,sy;
    // first step
    const int N_points_6 = 6;

    tk::spline sx6,sy6;
    // last step
    const int N_points_7 = 7;

    tk::spline sx7,sy7;

    vector<double> X(N_points), Y(N_points), T(N_points);
    vector<double> X6(N_points_6),Y6(N_points_6),T6(N_points_6);
    vector<double> X7(N_points_7),Y7(N_points_7),T7(N_points_7);

    const double x0_foot = 0;
    const double y0_foot = 0;
    const float step_length_scaling = 1.0;
    const float step_height_scaling = 1.0;

    // swing phase begin && stance phase end
    X[1]= x0_foot+step_length_scaling*(0.05835);
    X[2]= x0_foot+step_length_scaling*(-0.06862);
    X[3]= x0_foot+step_length_scaling*(-0.1826);
    X[4]= x0_foot+step_length_scaling*(-0.1445);
    X[5]= x0_foot+step_length_scaling*(0.0578);
    X[6]= x0_foot+step_length_scaling*0.2623;
    X[7]= x0_foot+step_length_scaling*0.3755;
    X[8]= x0_foot+step_length_scaling*0.3923;
    X[9]= x0_foot+step_length_scaling*0.369;
    
    //swing phase end && stance phase begin
    X[10]= x0_foot+step_length_scaling*0.3572;
    X[11]= x0_foot+step_length_scaling*0.3491;
    X[12]= x0_foot+step_length_scaling*0.2764;
    X[13]=x0_foot+step_length_scaling*0.16;
    X[14]= x0_foot+step_length_scaling*0.1454;
    X[15]= x0_foot+step_length_scaling*0.1184;
    X[16]= x0_foot+step_length_scaling*0.09285;
    X[17]=X[1];
    // additional points to assure periodicity
    X[18]=X[2];
    X[0]= X[16];


    // swing phase begin && stance phase end
    Y[1]= y0_foot+step_height_scaling*(-1.029);
    Y[2]= y0_foot+step_height_scaling*(-1.017);
    Y[3]= y0_foot+step_height_scaling*(-0.946);
    Y[4]= y0_foot+step_height_scaling*(-0.8785);
    Y[5]= y0_foot+step_height_scaling*(-0.9152);
    Y[6]= y0_foot+step_height_scaling*(-0.9578);
    Y[7]= y0_foot+step_height_scaling*(-0.9448);
    Y[8]= y0_foot+step_height_scaling*(-0.9446);
    Y[9]= y0_foot+step_height_scaling*(-0.9558);
    // swing phase end && stance phase begin
    Y[10]=y0_foot+step_height_scaling*(-0.9591);
    Y[11]=y0_foot+step_height_scaling*(-0.9647);
    Y[12]=y0_foot+step_height_scaling*(-0.9895);
    Y[13]=y0_foot+step_height_scaling*(-1.017);
    Y[14]=y0_foot+step_height_scaling*(-1.017);
    Y[15]=y0_foot+step_height_scaling*(-1.022);
    Y[16]=y0_foot+step_height_scaling*(-1.023);
    Y[17]= Y[1];
    // additional points to assure periodicity
    Y[18]= Y[2];
    Y[0]= Y[16];

    // swing phase begin && stance phase end
    T[1]=0;
    T[2]=0.0625*T_Gait;
    T[3]=0.125*T_Gait;
    T[4]=0.1875*T_Gait;
    T[5]=0.25*T_Gait;
    T[6]=0.3125*T_Gait;
    T[7]=0.375*T_Gait;
    T[8]=0.4375*T_Gait;
    T[9]=0.5*T_Gait;
    // swing phase end && stance phase begin
    T[10]=0.5625*T_Gait;
    T[11]=0.625*T_Gait;
    T[12]=0.6875*T_Gait;
    T[13]=0.75*T_Gait;
    T[14]=0.8*T_Gait; // to be consistent 0.8125*T_Gait; but problem with state definition
    T[15]=0.875*T_Gait;
    T[16]=0.9375*T_Gait;
    T[17]=T_Gait;
    // additional points to assure periodicity
    T[18]=T_Gait+T[2];
    T[0]=-0.0625*T_Gait;

////////////////////////////////////////////////////////
#if FS
    // match end points to stand position (0.8 T_Gait) and double stand right foot in front
    X6[0]= X[14];
    X6[1]= (X[5]-X[14])/2+X[14];
    X6[2]= X[6];
    X6[3]= X[7];
    X6[4]= X[8];
    X6[5]= X[9];

    Y6[0]= Y[14];
    Y6[1]= (Y[5]-Y[14])/2+Y[14];
    Y6[2]= Y[6];
    Y6[3]= Y[7];
    Y6[4]= Y[8];
    Y6[5]= Y[9];

    // attention when evaluating using pseudo time which is normally used modulus T_Gait
    T6[0]=0.8*T_Gait;
    T6[1]=0.94*T_Gait;
    T6[2]=T_Gait + 0.08*T_Gait;
    T6[3]=T_Gait + 0.22*T_Gait;    
    T6[4]=T_Gait + 0.36*T_Gait;
    T6[5]=T_Gait + 0.5*T_Gait;
#endif
/////////////////////////////////////////////////////////    

#if LS
    // match end points to double stand right foot in front and stand position (0.8 T_Gait)
    
    X7[0]= X[0];
    X7[1]= X[1];
    X7[2]= X[2];
    X7[3]= X[3];
    X7[4]= X[4];
    X7[5]= X[5];
    X7[6]= X[14];

    Y7[0]= Y[0];
    Y7[1]= Y[1];
    Y7[2]= Y[2];
    Y7[3]= Y[3];
    Y7[4]= Y[4];
    Y7[5]= Y[5];
    Y7[6]= Y[14];

    T7[0]=-0.16*T_Gait;
    T7[1]=0.;
    T7[2]=0.16*T_Gait;
    T7[3]=0.32*T_Gait;
    T7[4]=0.48*T_Gait;
    T7[5]=0.64*T_Gait;
    T7[6]=0.8*T_Gait;
#endif
////////////////////////////////////////////////////////    


    sx.set_points(T,X);
    sy.set_points(T,Y);
#if FS    
    sx6.set_points(T6,X6);
    sy6.set_points(T6,Y6);
#endif
#if LS    
    sx7.set_points(T7,X7);
    sy7.set_points(T7,Y7);
#endif    
        
    

    const char * filename = "xy.dat";
    ofstream outfile(filename);
    if (!outfile.is_open()) {
   		cout << "Could not open file " << filename << endl;
       	// abort
        return 1;
    }

    for(int i=0; i<X.size(); i++)
    {
	    outfile << X[i] << " " << Y[i] << endl;
	}
	outfile.close();

    const char * filename2 = "xy_spline.dat";
    ofstream outfile2(filename2);
    if (!outfile2.is_open()) {
   		cout << "Could not open file " << filename2 << endl;
       	// abort
        return 1;
    }

    for(int i=0; i<T_Gait*100; i++)
    {
        double t=0.01*i;
	    outfile2 << sx(t) << " " << sy(t) << endl;
    }
	outfile2.close();


    const char * filename3 = "phi_knee.dat";
    ofstream outfile3(filename3);
    if (!outfile3.is_open()) {
   		cout << "Could not open file " << filename3 << endl;
       	// abort
        return 1;
    }
    const char * filename4 = "phi_hip.dat";
    ofstream outfile4(filename4);
    if (!outfile4.is_open()) {
   		cout << "Could not open file " << filename4 << endl;
       	// abort
        return 1;
    }

    for(int i=0; i<T_Gait*100; i++)
    {
        double t=0.01*i;
        const double phi_torso = 0; 
        double x_foot = sx(t);
        double y_foot = sy(t);
	    double l2 = 0.48225;
        double l3 = 0.551225;
  
	    double c2 = (x_foot*x_foot + y_foot*y_foot - l2*l2 - l3*l3)/(2*l2*l3);
        if (c2*c2>1.)
        {
        	c2=1;
        }
        double s2 = sqrt(1 - c2*c2);
        double phi_knee = atan2(s2, c2); // theta2 is deduced
  
        double k1 = l2 + l3*c2;
        double k2 = l3*s2;
        double phi_hip =  (atan2(y_foot,x_foot) + atan2(k2, k1)) + phi_torso+M_PI/2;	
	    outfile3 << t << " " << phi_knee/M_PI*180 << endl;
        outfile4 << t << " " << phi_hip/M_PI*180 << endl;
    }
	outfile3.close();
    outfile4.close();
    

#if FS
    const char * filename5 = "xy6.dat";
    ofstream outfile5(filename5);
    if (!outfile5.is_open()) {
   		cout << "Could not open file " << filename5 << endl;
       	// abort
        return 1;
    }

    for(size_t i=0; i<X6.size(); i++)
    {
	    outfile5 << X6[i] << " " << Y6[i] << endl;
	}
	outfile5.close();

    const char * filename6 = "xy6_spline.dat";
    ofstream outfile6(filename6);
    if (!outfile6.is_open()) {
   		cout << "Could not open file " << filename6 << endl;
       	// abort
        return 1;
    }

    for(float i=T_Gait*0.8; i<T_Gait+0.5*T_Gait; i=i+0.1)
    {
        double t=i;
	    outfile6 << sx6(t) << " " << sy6(t) << endl;
    }
	outfile6.close();

    const char * filename7 = "phi_knee_6.dat";
    ofstream outfile7(filename7);
    if (!outfile7.is_open()) {
   		cout << "Could not open file " << filename7 << endl;
       	// abort
        return 1;
    }
    const char * filename8 = "phi_hip_6.dat";
    ofstream outfile8(filename8);
    if (!outfile8.is_open()) {
   		cout << "Could not open file " << filename8 << endl;
       	// abort
        return 1;
    }

    for(float i=T_Gait*0.8; i<T_Gait+0.5*T_Gait; i=i+0.1)
    {
        double t=i;
        const double phi_torso = 0; 
        double x_foot = sx6(t);
        double y_foot = sy6(t);
	    double l2 = 0.48225;
        double l3 = 0.551225;
  
	    double c2 = (x_foot*x_foot + y_foot*y_foot - l2*l2 - l3*l3)/(2*l2*l3);
        if (c2*c2>1.)
        {
        	c2=1;
        }
        double s2 = sqrt(1 - c2*c2);
        double phi_knee = atan2(s2, c2); // theta2 is deduced
  
        double k1 = l2 + l3*c2;
        double k2 = l3*s2;
        double phi_hip =  (atan2(y_foot,x_foot) + atan2(k2, k1)) + phi_torso+M_PI/2;	
	    outfile7 << t << " " << phi_knee/M_PI*180 << endl;
        outfile8 << t << " " << phi_hip/M_PI*180 << endl;
    }
	outfile7.close();
    outfile8.close();
#endif
#if LS    
    const char * filename9 = "xy7.dat";
    ofstream outfile9(filename9);
    if (!outfile9.is_open()) {
   		cout << "Could not open file " << filename9 << endl;
       	// abort
        return 1;
    }

    for(size_t i=0; i<X7.size(); i++)
    {
	    outfile9 << X7[i] << " " << Y7[i] << endl;
	}
	outfile9.close();

    const char * filename10 = "xy7_spline.dat";
    ofstream outfile10(filename10);
    if (!outfile10.is_open()) {
   		cout << "Could not open file " << filename10 << endl;
       	// abort
        return 1;
    }

    for(float i=0; i<0.8*T_Gait; i=i+0.1)
    {
        double t=i;
	    outfile10 << sx7(t) << " " << sy7(t) << endl;
    }
	outfile7.close();

    const char * filename11 = "phi_knee_7.dat";
    ofstream outfile11(filename11);
    if (!outfile11.is_open()) {
   		cout << "Could not open file " << filename11 << endl;
       	// abort
        return 1;
    }
    const char * filename12 = "phi_hip_7.dat";
    ofstream outfile12(filename12);
    if (!outfile12.is_open()) {
   		cout << "Could not open file " << filename12 << endl;
       	// abort
        return 1;
    }

    for(float i=0; i<0.8*T_Gait; i=i+0.1)
    {
        double t=i;
        const double phi_torso = 0; 
        double x_foot = sx7(t);
        double y_foot = sy7(t);
	    double l2 = 0.48225;
        double l3 = 0.551225;
  
	    double c2 = (x_foot*x_foot + y_foot*y_foot - l2*l2 - l3*l3)/(2*l2*l3);
        if (c2*c2>1.)
        {
        	c2=1;
        }
        double s2 = sqrt(1 - c2*c2);
        double phi_knee = atan2(s2, c2); // theta2 is deduced
  
        double k1 = l2 + l3*c2;
        double k2 = l3*s2;
        double phi_hip =  (atan2(y_foot,x_foot) + atan2(k2, k1)) + phi_torso+M_PI/2;	
	    outfile11 << t << " " << phi_knee/M_PI*180 << endl;
        outfile12 << t << " " << phi_hip/M_PI*180 << endl;
    }
	outfile11.close();
    outfile12.close();
#endif    

	
	return EXIT_SUCCESS;
}


