#ifndef Intercom_datenpacket_HEADER
#define Intercom_datenpacket_HEADER


#include <vector>
#include "IDatenPacket.h"

//Rohes Datenpacket in welchem pro float ein byte zur ferf�gung steht um den zweck zu definieren
class Intercom_datenpacket : IDatenPacket
{
public:



	std::vector<unsigned char> com;
	std::vector<float> data;

	

	//Zeit in milisekunden seit start
	unsigned long time;

	void setTime(long ti);

	Intercom_datenpacket(long anzahlDaten, unsigned long zeit);

	Intercom_datenpacket();

	long getAnzahlBytes();

	unsigned char getID();

	unsigned long getTime();

	std::vector<unsigned char> convertToByte();

	void convertFromByte(std::vector<unsigned char>  msg);

private:
	//Anzahl daten
	long anzahl_daten;
	//Anzahl Bytes im Packet
	long anzahl_bytes;
	


};

#endif
