#include "Output_Datenpacket.h"
#include <vector>
#include <cstring>


using namespace std;

	

	Output_Datenpacket::Output_Datenpacket()
	{
		//Anzahl bytes in einem Output struct
		this->anzahl_bytes = 10;
			//16 Leds die �ber je ein bit gesteuert werden
		this->led = std::vector<unsigned char>(2);
	
		//Value of the vibration motors byte 0 is right byte 1 is left
		this->vib_motor = std::vector<unsigned char>(2);

		//On board sound tweeter
		this->tweeter = 0;

		//Zeit in milisekunden
		this->time=0;
	}

	Output_Datenpacket::Output_Datenpacket(unsigned long zeit)
	{

		//On board sound tweeter
		this->tweeter = 0;
		this->time = zeit;

		//Anzahl bytes in einem Output struct
		this->anzahl_bytes = 10;
		//16 Leds die �ber je ein bit gesteuert werden
			//16 Leds die �ber je ein bit gesteuert werden
		this->led = std::vector<unsigned char>(2);


		//Value of the vibration motors byte 0 is right byte 1 is left
		this->vib_motor = std::vector<unsigned char>(2);
\


		

		
	}

	unsigned long Output_Datenpacket::getTime(){
		return time;
	}

	std::vector<unsigned char>  Output_Datenpacket::convertToByte()
	{
		std::vector<unsigned char> packet(anzahl_bytes);

		packet[0] = led[0];
		packet[1] = led[1];
		packet[2] = vib_motor[0];
		packet[3] = vib_motor[1];
		packet[4] = tweeter;


		std::vector<unsigned char>  msg = createHeader(packet,time,2);

		return msg;
	}


	void Output_Datenpacket::convertFromByte(std::vector<unsigned char>  msg)
	{

		std::vector<unsigned char>  packet = detachHeader(msg);
		this->time = extractTime(msg);

		this->led[0] = packet[0];
		this->led[1] = packet[1];
		this->vib_motor[0] = packet[2];
		this->vib_motor[1] = packet[3];
		this->tweeter = packet[4];
	}

	//Nummer 0-15 and sets the led whit value state 0,1
	void Output_Datenpacket::setLED(int number, bool state)
	{

		int byt = number / 4;
		int bit = number % 4;
		if (state)
		{
			led[byt] |= (unsigned char)(1 << bit);
		}
		else
		{
			led[byt] &= (unsigned char)(0xff ^ (1 << number));
		}


	}

	bool Output_Datenpacket::getLEDstate(int number)
	{
		int byt = number / 4;
		int bit = number % 4;
		int res = led[byt] & (unsigned char)(1 << bit);

		if (res == 0)
		{
			return false;
		}

		return true;
	}

	unsigned char Output_Datenpacket::getID()
	{

		return 2;
	}

