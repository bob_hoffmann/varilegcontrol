#include "Packet_Reciever.h"
#include "IDatenPacket.h"
#include "Intercom_datenpacket.h"
#include "Output_Datenpacket.h"
#include "Sensor_Datenpacket.h"
#include "Commands_Datenpacket.h"
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <zmq.hpp>
#include <unistd.h>

// Bob's includes
#include <math.h>
#include "control_global_variables.h"
#include "sensor_processing.h"
#include "spline.h"
#include "control_functions.h"

using namespace std;

Packet_Reciever rec;
zmq::context_t *context;
zmq::socket_t *socket;
bool first_receive = true;
bool slave4safe = false;


void DecodeRecMsg(std::vector<unsigned char> msg);


void rec_msg(){

	zmq::message_t request;
	socket->recv(&request);
	vector<unsigned char> msg(request.size());
	memcpy((void *)msg.data(), request.data(), request.size());
	//std::cout <<(int) msg[0]<<endl;
	DecodeRecMsg(msg);
	return;

}


void send(std::vector<unsigned char>  msg){

	 zmq::message_t reply( msg.size() );
	 memcpy((void *)reply.data(), msg.data(), msg.size());
	 socket->send(reply);
	

}

void init_zmq(){

	 context = new zmq::context_t(1);
	 socket = new zmq::socket_t(*context, ZMQ_PAIR);
	 socket->connect("tcp://*:5555");

 }







/*
 * Code fuer BA und Controlling und so hier in die Event getriggerten Funktionen schreiben
*/

//bool trigger1=0;

void OnInterRec(Intercom_datenpacket cd){
	
	cout<<"cd.data[1]: "<< cd.data[1]<<endl;
    for(unsigned int i=0; i<cd.com.size();i++)
        if(cd.com[i] == 20 && cd.data[i]==1)
		{
            		buttons[0] = 1;
			buttons[1] = 0;
			buttons[2] = 0;
		}

	cout<<"cd.data[1]: "<< cd.data[1]<<endl;
    for(unsigned int i=0; i<cd.com.size();i++)
        if(cd.com[i] == 21 && cd.data[i]==1)
		{
            		buttons[0] = 0;
			buttons[1] = 1;
			buttons[2] = 0;
		}

	cout<<"cd.data[1]: "<< cd.data[1]<<endl;
    for(unsigned int i=0; i<cd.com.size();i++)
        if(cd.com[i] == 22 && cd.data[i]==1)
		{
            		buttons[0] = 0;
			buttons[1] = 0;
			buttons[2] = 1;
		}
    cout<<"cd.data[1]: "<< cd.data[1]<<endl;
	for(unsigned int i=0; i<cd.com.size();i++)
        if(cd.com[i] == 1 && cd.data[i]==1)
		{
            slave4safe = true;
		}
}

void OnComRec(Commands_Datenpacket cod){


}


void OnOutRec(Output_Datenpacket odp){
	cout<<(int)odp.tweeter<<endl;
}


void OnSensRec(Sensor_Datenpacket sd){

    	cout<<"on sensor receive"<<endl;

	if(first_receive)										//Marc : Initialisation of the time
	{
		begin_time = std::chrono::steady_clock::now();
		first_receive = false;
	}

	//end time measurement							
    	end_time= std::chrono::steady_clock::now();
	
    	long duration = chrono::duration_cast<chrono::microseconds>(end_time - begin_time).count(); // [microseconds]
    	//start time measurement
	begin_time = std::chrono::steady_clock::now();

    	// Bob's sensor_processing
    	sensor_processing sp(sd.IMU, sd.LOADCELL, sd.MOTORENWERTE, sd.HOTPOT);
        // Elements can now be accessed using sp.GRF , sp.dphi , etc.

	// Main safety 
	if(safe_state_call(sp, sd, slave4safe))  //-> kill all
	{
		Intercom_datenpacket *id = new Intercom_datenpacket();
		id->com[1] = 2; 
		id->data[1] = 1; 
		send(id->convertToByte());
		delete id;
		slave4safe = true;
	};

    	// Bob's state machine
	state = state_machine(/*, sp.GRF*/ sp.joint_phi,/* sp.absolute_psi, */ state_prev,pseudo_time_right,pseudo_time_left);
				

    	// Bob's time
    	virtual_time = virtual_time + duration;

    	// Bob's time generator
    	pseudo_time_left = time_generator_left(pseudo_time_left_prev, duration, state);
   	pseudo_time_right = time_generator_right(pseudo_time_right_prev, duration, state);

   	// Bob's Trajectory Generator
    	vector<vector<fp_value> > calc_motorpositions(6);
    	vector<vector<int> > int_motorpositions(6);
    	for(unsigned int i=0; i < 6;i++)
    	{
        	calc_motorpositions[i].resize(number_motor_values);
        	int_motorpositions[i].resize(number_motor_values);
    	}
    	calc_motorpositions = trajectory_generator(pseudo_time_left, pseudo_time_right, state);    		
	motor_security_check(calc_motorpositions, sp, sd);
	int_motorpositions = conversions(calc_motorpositions);

    // save old values; "feedback values"
    state_prev = state;
    pseudo_time_left_prev = pseudo_time_left;
    pseudo_time_right_prev = pseudo_time_right;


    // display values
    cout<<"Duration: "<<duration<<"\t virtual time "<<virtual_time<<endl;
    for(int i=0;i<6;i++)
    {
        for(int j=0;j<1;j++)
        {
            cout<<"Motor "<<i<<" at Motorposition "<< j << ": " << int_motorpositions[i][j] << "\t" << calc_motorpositions[i][j] << "\n";		
            	
        }
    }
    
	if(slave4safe) cout<<"SAFE MODE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
    cout<<"State"<<state<<"\n";
    cout<<"pseudo_time_right \t"<<pseudo_time_right<<"\n";
    cout<<"pseudo_time_left \t"<<pseudo_time_left<<"\n";    

    cout<<"knee left\t"<<sd.HOTPOT[0]<<endl;
    cout<<"lever left\t"<<sd.HOTPOT[1]<<endl;
    cout<<"winch left\t"<<sd.HOTPOT[2]<<endl;
    cout<<"knee right\t"<<sd.HOTPOT[3]<<endl;
    cout<<"lever right\t"<<sd.HOTPOT[4]<<endl;
    cout<<"winch right\t"<<sd.HOTPOT[5]<<endl;
    cout<<"hip left\t"<<sd.HOTPOT[6]<<endl;
    cout<<"hip right\t"<<sd.HOTPOT[7]<<endl;

    

	/* TESTING */
    /*
	if(virtual_time > (7*T_Gait/2))
	{
		Intercom_datenpacket *id = new Intercom_datenpacket();

		id->com[1] = 22; //P anteil Hüfte links
		id->data[1] = 1; //Wert 

		send(id->convertToByte());
		delete id;
	}
	else	if(virtual_time > (5*T_Gait/2))
			{
				Intercom_datenpacket *id = new Intercom_datenpacket();

				id->com[1] = 21; 
				id->data[1] = 1; 

				send(id->convertToByte());
				delete id;
			}
			else	if(virtual_time > (3*T_Gait/2))
					{
						Intercom_datenpacket *id = new Intercom_datenpacket();

						id->com[1] = 20; 
						id->data[1] = 1; 

						send(id->convertToByte());
						delete id;
					}
					else	if(virtual_time > (T_Gait/2))	
							{
								Intercom_datenpacket *id = new Intercom_datenpacket();

								id->com[1] = 21; 
								id->data[1] = 1; 
								send(id->convertToByte());
								delete id;
							}

    */
	/* TESTING */
	

    	// command package
	Commands_Datenpacket *cod=new Commands_Datenpacket(3);
    	cod->motorposition=int_motorpositions;

	send(cod->convertToByte());
	delete cod;
}





int main(int argc, const char* argv[])
{
	//cout<<"Hello?"<<endl;
	init_zmq();
    //begin_time = std::chrono::steady_clock::now();

	control_init(first_receive, slave4safe);
	while(true){
		rec_msg();

		/* Code for sending
		Intercom_datenpacket *icd=new Intercom_datenpacket(6,5);
		send(icd->convertToByte());
		delete icd;
		*/
		

	}
	

}







void DecodeRecMsg(std::vector<unsigned char> msg){





	if(msg[0]==1){
		OnSensRec(rec.reciveSensor(msg));
	
	}
	if(msg[0]==2){
		OnOutRec(rec.reciveOutput(msg));

	}

	if(msg[0]==3){
		OnComRec(rec.reciveComand(msg));

	}
	if(msg[0]==4){
		OnInterRec(rec.reciveIntercom(msg));

	
	}

}





 




