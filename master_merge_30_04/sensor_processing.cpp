// put in varileginterface

#include "sensor_processing.h"

sensor_processing::sensor_processing(const vector<vector<float>> & imu, const vector<vector<float>> & loadcell, const vector<vector<float>> & motorenwerte, const vector<float> & hotpot)
{
	// initialize INPUT VECTORS
	IMU.resize(MEMORYSIZE);
	for (unsigned int k=0; k < MEMORYSIZE; ++k)
	{
		IMU[k].resize(imu.size());
		for (unsigned int i=0; i < imu.size(); ++i)
			IMU[k][i].resize(imu[i].size());
		IMU[k] = imu;
	}

	LOADCELL.resize(MEMORYSIZE);
	for (unsigned int k=0; k < MEMORYSIZE; ++k)
	{
		LOADCELL[k].resize(loadcell.size());
		for (unsigned int i=0; i < loadcell.size(); ++i)
			LOADCELL[k][i].resize(loadcell[i].size());
		LOADCELL[k] = loadcell;
	}

	RPM.resize(MEMORYSIZE);
	for (unsigned int k=0; k < MEMORYSIZE; ++k)
	{
		RPM[k].resize(motorenwerte.size());
		for (unsigned int i=0; i < motorenwerte.size(); ++i)
			RPM[k][i] = motorenwerte[i][0];
	}

	HOTPOT.resize(MEMORYSIZE);
	for (unsigned int k=0; k < MEMORYSIZE; ++k)
	{
		HOTPOT[k].resize(hotpot.size());
		HOTPOT[k] = hotpot;
	}

	// initialize OUTPUT VECTORS
/*	for (unsigned int i; i < imu.size(); ++i)
		for (unsigned int j; j < imu.size[i](); ++j)
			for (unsigned int k; k < MEMORYSIZE; ++k)
				filtered_IMU_old[i][j][k] = imu[i][j]; */
	loadcell_old.resize(MEMORYSIZE);
	for (unsigned int k=0; k < MEMORYSIZE; ++k)
	{
		loadcell_old[k].resize(loadcell.size());
		for (unsigned int i=0; i < loadcell.size(); ++i)
			loadcell_old[k][i].resize(loadcell[i].size());
		loadcell_old[k] = loadcell;
	}

	RPM_old.resize(MEMORYSIZE);
	for (unsigned int k=0; k < MEMORYSIZE; ++k)
	{
		RPM_old[k].resize(motorenwerte.size());
		for (unsigned int i=0; i < motorenwerte.size(); ++i)
			RPM_old[k][i] = motorenwerte[i][0];
	}

	joint_phi_old.resize(MEMORYSIZE);
	for (unsigned int k=0; k < MEMORYSIZE; ++k)
	{
		joint_phi_old[k].resize(hotpot.size());
		joint_phi_old[k] = hotpot;
	}
	
	// initialize "output variables"
    joint_phi.resize(6); 
    //absolute_psi.resize(7);
    dphi.resize(6);
    ddphi.resize(6);
}

// Process the sensor data
void sensor_processing::process(const vector<vector<float>> & imu, const vector<vector<float>> & loadcell, const vector<vector<float>> & motorenwerte, const vector<float> & hotpot)
{
	// Initialisation
	IMU[0] = imu;
	LOADCELL[0] = loadcell;
	for (unsigned int i=0; i < motorenwerte.size(); ++i)
		RPM[0][i] = motorenwerte[i][0];
	HOTPOT[0] = hotpot;

	//dt_filter(IMU, filtered_IMU_old);						// Marc Commentar: DO NOT FORGET THE SECOND ARGUMENT OF THE DT_FILTER
	dt_filter(LOADCELL, loadcell_old);
    dt_filter(RPM,RPM_old);
    dt_filter(HOTPOT,joint_phi_old);

    loadcell_2_GRF();
    sensor_fusion();

}

void sensor_processing::dt_filter(vector<vector<vector<float>>> & data, vector<vector<vector<float>>> & filtered_data, const int parameter)	// Marc Commentar: What about to transmit the "data" as reference? I think this would be more efficient, isn't it?
{
	// To design IIR-LowPass filter use MatLab:
	// polybox : 002 Technische Daten/005 Sensor/HotPot_Sensofoil/Sensor_READ_OUT.m
	// Use a and b given by MatLab
	// Has to be adjusted, for frequency and ...

	if (parameter != 0)
	{
		float a[] = { 1.000, -1.1430, 0.4128 };
		float b[] = { 0.0675, 0.1349, 0.0675 };

		// do some filtering
		for (unsigned int i = 0; i < data[0].size(); ++i)
			for (unsigned int j = 0; j < data[0][i].size(); ++j)
			{
				float B_term = 0;
				float A_term = 0;
				for (unsigned int k = 0; k < MEMORYSIZE; ++k)
					B_term += b[k] * data[k][i][j];
				for (unsigned int k = 1; k < MEMORYSIZE; ++k)
				{
					A_term += a[k] * filtered_data[k][i][j];
					filtered_data[k][i][j] = filtered_data[k - 1][i][j];
				}
				filtered_data[0][i][j] = 1. / a[0] * (B_term - A_term);
			}
	}
	else
	{
		for (unsigned int k = 1; k < MEMORYSIZE; ++k)
			filtered_data[k] = filtered_data[k - 1];
		filtered_data[0] = data[0];
	}
    // depending on the parameter

	//Sending old values into the memory
	for (unsigned int k = 1; k < MEMORYSIZE; ++k)
		data[k] = data[k - 1];	
}

void sensor_processing::dt_filter(vector<vector<float>> & data, vector<vector<float>> & filtered_data, const int parameter) // Marc Commentar: What about to transmit the "data" as reference? I think this would be more efficient, isn't it?
{
	// To design IIR-LowPass filter use MatLab:
	// polybox : 002 Technische Daten/005 Sensor/HotPot_Sensofoil/Sensor_READ_OUT.m
	// Use a and b given by MatLab
	// Has to be adjusted, for frequency and ...


	if (parameter != 0)
	{
		float a[] = { 1.000, -1.1430, 0.4128 };
		float b[] = { 0.0675, 0.1349, 0.0675 };

		// do some filtering
		for (unsigned int i = 0; i < data[0].size(); ++i)
			{
				float B_term = 0;
				float A_term = 0;
				for (unsigned int k = 0; k < MEMORYSIZE; ++k)
					B_term += b[k] * data[k][i];
				for (unsigned int k = 1; k < MEMORYSIZE; ++k)
				{
					A_term += a[k] * filtered_data[k][i];
					filtered_data[k][i] = filtered_data[k - 1][i];
				}
				filtered_data[0][i] = 1. / a[0] * (B_term - A_term);
			}
	}
	else
	{
		for (unsigned int k = 1; k < MEMORYSIZE; ++k)
			filtered_data[k] = filtered_data[k - 1];
		filtered_data[0] = data[0];
	}
	// depending on the parameter

	//Sending old values into the memory
	for (unsigned int k = 1; k < MEMORYSIZE; ++k)
		data[k] = data[k - 1];
}

// Define return type!
void sensor_processing::loadcell_2_GRF()
{
	enum foot{ right = 0, left };
	enum position{ front = 0, mid_fro, mid_back, back };

	// HAS TO BE ADJUSTED
	const float MIN = 500;	// Minimal value, higher values mean some sort of contact (value when foot is not in contact) //THRESHOLD	

	float max_GRF = 0;	// Maximal GRF measured in this cycle
	bool contactRight[4] = { 0 }, contactLeft[4] = { 0 };	// Which loadcell has contact
	unsigned int count_right = 0, count_left = 0;		// How many loadcells have contact

	// Check for meaningful values and retrieve data
	for (unsigned int i = 0; i < loadcell_old[0][right].size(); i++)
	{
		// Right foot
		if (loadcell_old[0][right][i] < 0 || loadcell_old[0][right][i] > 4096)
		{
			// report error (in some logfile or so)
			// continue
		}
		else if (loadcell_old[0][right][i] > MIN){
			contactRight[i] = true;	// Report contact
			count_right++;
		}

		if (loadcell_old[0][right][i] > max_GRF)
		{
			max_GRF = loadcell_old[0][right][i];	//Find greatest GRF
		}

		// Left foot
		if (loadcell_old[0][left][i] < 0 || loadcell_old[0][left][i] > 4096)
		{
			// report error
			// continue
		}
		else if (loadcell_old[0][left][i] > MIN){
			contactLeft[i] = true;	// Report contact
			count_left++;
		}

		if (loadcell_old[0][left][i] > max_GRF)
		{
			max_GRF = loadcell_old[0][left][i];	//Find greatest GRF
		}
	}

	if (count_right >= 2)
		GRF.onGround_right = true;
	else GRF.onGround_right = false;
	
	if (count_left >= 2)
		GRF.onGround_left = true;
	else GRF.onGround_left = false;

	/*// Elaborate retrieved data
	// ------------------------

	// 0 = standing, 1 = right swing, 2 = left swing, 3 = error
	unsigned int stateID = 3;

	if (count_right && count_left) stateID = 0;
	else if (count_right) stateID = 1;
	else if (count_left) stateID = 2;
	else{
		state_ID = 3; // perhaps not needed
		// report error
	} */
	// Choose what you want to return!!

	// possible feedback struct:
	/*struct status{
	bool onGround_left;
	bool onGround_right;
	float max_GRF;
	int id_max_GRF[2];
	// more information possible
	} footStatus;*/

	/* // Marc Commentar: Idea: Transmit all the data of the loadcell from one foot to this function. If 2 of these loadcells are greater than a defined threshold, then GRF=1 else GRF=0
	GRF = LOADCELL;		// At the end: fill GRF with new values		*/																																			
}

void sensor_processing::sensor_fusion() // Marc Commentar: We have to discuss this, because I don't get the idea of this.
{	
    // do something clever for all output variables
	for (unsigned int i=0; i < joint_phi_old[0].size(); ++i)
		joint_phi[i] = joint_phi_old[0][i];
	for (unsigned int i=0; i < RPM_old[0].size(); ++i)
		dphi[i] = RPM_old[0][i];

    /*for(unsigned int i = 0 ; i < MOTORENWERTE.size() ; i++ )
    {
        dphi.push_back(MOTORENWERTE[i][0]);
    }*/
    // read "An efficient orientation filter for inertial and inertial/magnetic sensor arrays"
    // or here: "http://www.freescale.com/files/sensors/doc/app_note/AN4248.pdf"
    
	
	//absolute_psi.push_back(0);
        
    ddphi.push_back(0);
}
