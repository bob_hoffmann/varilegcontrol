#ifndef IDatenPacket_HEADER2
#define IDatenPacket_HEADER2

#include <vector>


using namespace std;





 class IDatenPacket
{
public:
	 long getAnzahlBytes();
	 unsigned char getID(){return 0;};
	unsigned long getTime(){return 0;};
	void setTime(long ti){};
	 std::vector<unsigned char> convertToByte();
	 void convertFromByte(std::vector<unsigned char>  packet);

	unsigned long time;

	std::vector<unsigned char> createHeader(std::vector<unsigned char> msg, unsigned long time_t, unsigned char ID_t);

	unsigned long extractTime(std::vector<unsigned char> msg);

	std::vector<unsigned char> detachHeader(std::vector<unsigned char> msg);


 };


#endif
