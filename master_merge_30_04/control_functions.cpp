#include "control_global_variables.h"
#include "control_functions.h"

#include <vector>
#include "spline.h"
#include <math.h>
#include <iostream>
#include "sensor_processing.h"
#include "Sensor_Datenpacket.h"

using namespace std;

// Bob's function definitions
void control_init(bool & first_receive, bool & slave4safe)
{
    // Bob's initializations of global variables    
    state = stand_parallel;
    state_prev = stand_parallel;
	virtual_time = 0;		
	pseudo_time_left = 0.8*T_Gait;;
	pseudo_time_left_prev = 0.8*T_Gait;;
	pseudo_time_right = 0.8*T_Gait;;
	pseudo_time_right_prev = 0.8*T_Gait;;

    
    spline_setup();
    // end Bob's initializations of global variables
    first_receive = true;
    slave4safe = false;

}

int state_machine(/*const ground_status & GRF,*/vector<fp_value> joint_phi,/* vector<fp_value> absolute_psi ,*/ int id_prev, int t_pseudo_right, int t_pseudo_left) 
						
{
	if (id_prev == safe) return safe;
	else
	{
		bool button_left = buttons[0];
		bool button_right = buttons[1];
		bool button_stand = buttons[2];
		// reset buttons for safety
		buttons[0]=0;
		buttons[1]=0;
		buttons[2]=0;
		
				
		t_pseudo_right = t_pseudo_right % T_Gait;
		t_pseudo_left = t_pseudo_left % T_Gait;

		/*
		// detect GRF
		bool GRF_left = 0;
		bool GRF_right = 0;
		for (unsigned int i = 0; i < GRF[0].size(); i++)
		{
			if (GRF[1][i] > GRF_THRES)
			{
				GRF_left = 1;
			}
			if (GRF[0][i] > GRF_THRES)
			{
				GRF_right = 1;
			}

		}
		*/

		// Marc worked last on this:
		if (id_prev == stand_left_front && button_left == 0 && button_right == 1 && button_stand == 0 )//&& GRF.onGround_left == 1 && GRF.onGround_right == 1)
		{
			return right_swing;
		}
		else if (id_prev == stand_right_front && button_left == 1 && button_right == 0 && button_stand == 0 /* && GRF.onGround_left == 1 && GRF.onGround_right == 1 */ && id_prev != stand_parallel)
		{
			return left_swing;
		}
		else if (id_prev == stand_parallel && button_left == 0 && button_right == 1 && button_stand == 0 )//&& GRF.onGround_left == 1 && GRF.onGround_right == 1)
		{
			return right_swing_begin;
		}
		else if (id_prev == stand_right_front && button_left == 0 && button_right == 0 && button_stand == 1)// && GRF.onGround_left == 1 && GRF.onGround_right == 1)
		{
			return left_swing_end;
		}
		/* else if (id_prev == stand_parallel && button_left == 1 && button_right == 0 && button_stand == 0 && GRF.onGround_left == 1 && GRF.onGround_right ==1)
		{
		return left_swing_begin;
		}
		else if (id_prev == stand_left_front && button_left == 0 && button_right == 0 && button_stand == 1 && GRF.onGround_left == 1 && GRF.onGround_right ==1)
		{
		return right_swing_end;
		} */
		else if (t_pseudo_right == 0 && id_prev != left_swing_end && id_prev != right_swing_begin && t_pseudo_left!=0)
		{
			return stand_left_front;
		}
		else if (t_pseudo_left == 0 && t_pseudo_right != 0)
		{
			return stand_right_front;
		}
		else if (t_pseudo_right == t_pseudo_left)
		{
			return stand_parallel;
		}
		else
		{
			return id_prev;
		}
	}
}


long time_generator_left(long time_left_prev, long duration, int state)
{
    long y;
    long yprev = time_left_prev;
    switch(state)
    {
        case stand_left_front:
            y = T_Gait/2;
            break;
        
        case right_swing:
            if(yprev<T_Gait/2)
            {
                y=T_Gait/2+duration;
            }
            else if(yprev<T_Gait && yprev>T_Gait/2-0.01)
            {
                y = yprev+duration;
            }
            else
            {            
                y = T_Gait;
            }
            break;


        case stand_right_front:
            y = 0;
            break;
        
        case left_swing:
            if(yprev<T_Gait/2)
            {
                y = yprev+duration;
            }
            else
            {            
                y = T_Gait/2;
            }
            break;
            
        case stand_parallel:
            y = 0.8*T_Gait;
            break;
    
        case right_swing_begin:
            if(yprev>=0.8*T_Gait && yprev<T_Gait)
            {
                y = yprev+duration;
            }
            else
            {
                y = T_Gait;
            }
            break;
        
        case left_swing_end:
            if((yprev>=0 && yprev<0.8*T_Gait))
            {
                y = yprev+8./3.*duration;
                y = y%T_Gait;
            }
            else
            {
                y = 0.8*T_Gait;
            }
            break;

        /* case left_swing_begin:
            if(yprev<T_Gait/2)
            {
                y = yprev+duration;
            }
            else
            {            
                y = T_Gait/2;
            }
            break;
        
        case right_swing_end:
            if(yprev<T_Gait)
            {
                y = yprev+duration;
            }    
            else
            {            
                y = T_Gait;
            }
            break; */

        default:
            y = yprev;
    }
    return y;
}


long time_generator_right(long time_right_prev, long duration, int state)
{
    long y;
    long yprev = time_right_prev;
    switch(state)
    {
        case stand_left_front:
            y = 0;
            break;
        
        case right_swing:
            if(yprev<T_Gait/2)
            {
                y = yprev+duration;
            }
            else
            {            
                y = T_Gait/2;
            }
            break;

        case stand_right_front:
            y = T_Gait/2;
            break;
        
        case left_swing:
            if(yprev<T_Gait/2)
            {
                y=T_Gait/2+duration;
            }
            else if(yprev<T_Gait && yprev>=T_Gait/2)	
            {
                y = yprev+duration;
            }
            else
            {            
                y = T_Gait;
            }
            break;
            
        case stand_parallel:
            y = 0.8*T_Gait;
            break;
    
        case right_swing_begin:
            if((yprev>=0.8*T_Gait && yprev<T_Gait) || (yprev>=0 && yprev<T_Gait/2))		
            {
                y = yprev+7./2.*duration; // different time interval to cover
                y = y%T_Gait;
            }
            else
            {
                y = T_Gait/2;
            }
            break;
        
        case left_swing_end:
            if(yprev<0.8*T_Gait)
            {
                y = yprev+duration;
            }
            else
            {
                y = 0.8*T_Gait;
            }
            break;

        /* case left_swing_begin:
            if(yprev<T_Gait/2)
            {
                y = yprev+duration;
            }
            else
            {            
                y = T_Gait/2;
            }
            break;
        
        case right_swing_end:
            if(yprev<T_Gait)
            {
                y = yprev+duration;
            }    
            else
            {            
                y = T_Gait;
            }
            break; */
        default:
            y = yprev;
    }
    return y;
}


// Output type changed by Marc
vector<vector<fp_value> > trajectory_generator(long pseudo_time_left, long pseudo_time_right, int state)
{
    // calc next few motorpositions
    vector<vector<fp_value>>  motorposition(6);
    for(int i=0;i<6;i++)
    {
        motorposition[i].resize(number_motor_values);
    }
    
    // if state is not in fail state the next motor trajectories are calculated using the predefined task space trajectory
    if(state != safe)
    {
        // iterate through the number of motor values passed to the slave
        for(int i=0; i<number_motor_values ; i++)
        {
            // look up in spline --> x-y of each foot
            long time_motor_left = pseudo_time_left + i*reference_pos_freq;
            long time_motor_right = pseudo_time_right + i*reference_pos_freq;
            fp_value x_foot_left = 0;			
            fp_value y_foot_left = -1;
            fp_value x_foot_right = 0;
            fp_value y_foot_right = -1;

            if(state == right_swing_begin)
            {
                // compensate for how spline is defined
                if(time_motor_right<0.8*T_Gait)
                {
                    time_motor_right = time_motor_right + T_Gait;
                }
                x_foot_left = sx(time_motor_left);
                y_foot_left = sy(time_motor_left);
                x_foot_right = sx6(time_motor_right);
                y_foot_right = sy6(time_motor_right);
            }
            else if(state == left_swing_end)
            {
                x_foot_left = sx7(time_motor_left);
                y_foot_left = sy7(time_motor_left);
                x_foot_right = sx(time_motor_right);
                y_foot_right = sy(time_motor_right);              
            }
            else
            {
                x_foot_left = sx(time_motor_left);
                y_foot_left = sy(time_motor_left);
                x_foot_right = sx(time_motor_right);
                y_foot_right = sy(time_motor_right);
            }
            // do inverse kinematics to find phi knee and phi hip
            phi_inverse_kinematics phi_left = inverse_kinematics(x_foot_left,y_foot_left,Phi_Torso);
            phi_inverse_kinematics phi_right = inverse_kinematics(x_foot_right,y_foot_right,Phi_Torso);
            // assign to motorposition
            motorposition[3][i] = phi_left.phi_knee;
            motorposition[5][i] = phi_left.phi_hip;
            motorposition[0][i] = phi_right.phi_knee; // Marc's comment: I would call it phi_lever!
            motorposition[2][i] = phi_right.phi_hip;
				// pretension roll
            motorposition[1][i] = 0.55;		// [rad]
            motorposition[4][i] = 0.55;
        }
    }
    // do the conversion to int before passing motor values to slave
    // motorposition_int = conversions(motorposition);
    return motorposition;
}

// Marc end

// define x-y (t) positions of the foot during the normal gait 
void spline_setup()
{
    spline_walk();
    spline_first_step();
    spline_last_step();
}

void spline_walk()
{
    // swing phase begin && stance phase end   
    X[1]= x0_foot-0.22;
    X[2]= x0_foot-0.30;
    X[3]= x0_foot-0.02;
    X[4]= x0_foot+0.35;
    X[5]= x0_foot+0.52;
    X[6]= x0_foot+0.52;
    //swing phase end && stance phase begin
    X[7]= x0_foot+0.29;
    X[8]= x0_foot+0.15;
    X[9]= x0_foot+0.03;
    X[10]=x0_foot-0.12;
    X[11]=X[1];
    // additional points to assure periodicity
    X[12]=X[2];
    X[0]= X[10];


    // swing phase begin && stance phase end
    Y[1]= y0_foot+0.04;
    Y[2]= y0_foot+0.08;
    Y[3]= y0_foot+0.15;
    Y[4]= y0_foot+0.10;
    Y[5]= y0_foot+0.15;
    Y[6]= y0_foot+0.11;
    // swing phase end && stance phase begin
    Y[7]= y0_foot+0.05;
    Y[8]= y0_foot+0.01;
    Y[9]= y0_foot-0.01;
    Y[10]=y0_foot+0;
    Y[11]= Y[1];
    // additional points to assure periodicity
    Y[12]= Y[2];
    Y[0]= Y[10];

    // swing phase begin && stance phase end
    T[1]=0;
    T[2]=0.1*T_Gait;
    T[3]=0.2*T_Gait;
    T[4]=0.3*T_Gait;
    T[5]=0.4*T_Gait;
    T[6]=0.5*T_Gait;
    // swing phase end && stance phase begin
    T[7]=0.6*T_Gait;
    T[8]=0.7*T_Gait;
    T[9]=0.8*T_Gait;
    T[10]=0.9*T_Gait;
    T[11]=T_Gait;
    // additional points to assure periodicity
    T[12]=T_Gait+T[2];
    T[0]=-0.1*T_Gait;

    // Do spline in task space
    sx.set_points(T,X);
    sy.set_points(T,Y);
}

void spline_first_step()
{
    // match end points to stand position (0.8 T_Gait) and double stand right foot in front
    X6[0]= X[9];
    X6[1]= x0_foot+0.10;
    X6[2]= X[4];
    X6[3]= X[5];
    X6[4]= X[6];

    Y6[0]= Y[9];
    Y6[1]= y0_foot+0.05;
    Y6[2]= Y[4];
    Y6[3]= Y[5];
    Y6[4]= Y[6];

    // attention when evaluating using pseudo time which is normally used modulus T_Gait
    T6[0]=0.8*T_Gait;
    T6[1]=T_Gait;
    T6[2]=T_Gait + 0.2*T_Gait;
    T6[3]=T_Gait + 0.4*T_Gait;    
    T6[4]=T_Gait + 0.5*T_Gait;
    

    // Do spline in task space
    sx6.set_points(T6,X6);
    sy6.set_points(T6,Y6);    
}

void spline_last_step()
{
    // match end points to double stand right foot in front and stand position (0.8 T_Gait)
    X7[0]= X[1];
    X7[1]= X[2];
    X7[2]= X[3];
    X7[3]= x0_foot+0.1;
    X7[4]= X[9];

    Y7[0]= Y[1];
    Y7[1]= Y[2];
    Y7[2]= Y[3];
    Y7[3]= y0_foot+.08;
    Y7[4]= Y[9];

    T7[0]=0;
    T7[1]=0.2*T_Gait;
    T7[2]=0.4*T_Gait;
    T7[3]=0.6*T_Gait;
    T7[4]=0.8*T_Gait;

    // Do spline in task space
    sx7.set_points(T7,X7);
    sy7.set_points(T7,Y7);
}

phi_inverse_kinematics inverse_kinematics(fp_value x_foot, fp_value y_foot, fp_value Phi_Torso)
{
    phi_inverse_kinematics pik;

    // define lengths of the upper (l2) and lower leg (l3)
    const fp_value l2 = 0.48225;						// Marc Commentar: I would use const
    const fp_value l3 = 0.551225;
    
    // helper value for inverse kinematics
    fp_value c2 = (x_foot*x_foot + y_foot*y_foot - l2*l2 - l3*l3)/(2*l2*l3);
    // if value is out of task space, relocate value to the bound of the task space
    if (c2*c2>1.)
    {
        c2=1;
    }
    // helper value for inverse kinematics
    const fp_value s2 = sqrt(1 - c2*c2);				

    // phi knee is deduced
    pik.phi_knee=(atan2(s2, c2));
    
    // helper values for inverse kinematics  
    const fp_value k1 = l2 + l3*c2;					
    fp_value k2 = l3*s2;

    // phi hip is deduced
    pik.phi_hip=((atan2(y_foot,x_foot) + atan2(k2, k1)) + Phi_Torso+M_PI/2);

    return pik;
}

// As the motorvalues are converted from float to uint16
// a conversion has to be applied.
// -pi - pi is matched to 0-65536
vector<vector<int> > conversions(vector<vector<float> > motor_position)
{
    // iterate over 2D motor vector
    vector<vector<int> > motor_position_int(6);
    for(unsigned int i=0; i < 6;i++)
    {
        motor_position_int[i].resize(number_motor_values);
    }
    
    for(unsigned int i=0; i < 6;i++)
    {    
        for(unsigned int j=0; j < number_motor_values;j++)
        {
            // cout<<i<<j<<motor_position[i][j]<<endl;
            motor_position_int[i][j] = map(motor_position[i][j]);
        }
    }
    return motor_position_int;
}

//
int map(fp_value x, fp_value in_min, fp_value in_max, fp_value out_min, fp_value out_max) // mapping function , default values declared in header
{
	return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}

// end Bob's function definitions

// Marc's function definitions

void motor_security_check(vector<vector<fp_value> > & motorpositions, const sensor_processing & sp, const Sensor_Datenpacket & sd)
{
	//Initialisation
    
	const fp_value MAX_HIP = 1.5;
	const fp_value MIN_HIP = -0.1;
	const fp_value MAX_ROLL = 1.2;
	const fp_value MIN_ROLL = 0;
	const fp_value MAX_LEVER = 1.5;
	const fp_value MIN_LEVER = -0.2;
	const fp_value MAX_KNEE = 1.5;
	const fp_value MIN_KNEE = -0.1;
	
	for (int i = 0; i < number_motor_values; i++)
	{
		//check if pretension roll is in range for the values 0...Motorendatenpaket.size
		motorpositions[1][i] = actuator_joint_overstretch_protection(motorpositions[1][i], MAX_ROLL, MIN_ROLL);
		motorpositions[4][i] = actuator_joint_overstretch_protection(motorpositions[4][i], MAX_ROLL, MIN_ROLL);

		//check if lever is in range for the values 0...Motorendatenpaket.size
		motorpositions[0][i] = actuator_joint_overstretch_protection(motorpositions[0][i], MAX_LEVER, MIN_LEVER);
		motorpositions[3][i] = actuator_joint_overstretch_protection(motorpositions[3][i], MAX_LEVER, MIN_LEVER);

		//check if hip is in range for the values 0...Motorendatenpaket.size
		motorpositions[2][i] = actuator_joint_overstretch_protection(motorpositions[2][i], MAX_HIP, MIN_HIP);
		motorpositions[5][i] = actuator_joint_overstretch_protection(motorpositions[5][i], MAX_HIP, MIN_HIP);

		//compute the deflection length of the spring and check if it is not to long for 0..Motorendatenpacket --> release pretension roll
		motorpositions[1][i] = maximal_deflection_length_security(motorpositions[1][i], motorpositions[0][i], sp.joint_phi[3],1);	//right
		motorpositions[4][i] = maximal_deflection_length_security(motorpositions[4][i], motorpositions[3][i], sp.joint_phi[0],0); //left

		//check if pretension roll is in range for the values 0...Motorendatenpaket.size --> do something clever with the lever
			//NOT IMPLEMENTED

		//check if knee is in range for the values 0...Motorendatenpaket.size
		motorpositions[0][i] = knee_security_check(motorpositions[0][i], sp.joint_phi[4], sp.joint_phi[3], 1, MIN_KNEE, MAX_KNEE);	//right
		motorpositions[3][i] = knee_security_check(motorpositions[3][i], sp.joint_phi[1], sp.joint_phi[0], 0, MIN_KNEE, MAX_KNEE);	//left
	}
	//check if the gaps of the positions for the time steps are not to high 
		//NOT IMPLEMENTED
		//IDEA : USE A FILTER TO SMOOTH THE SIGNAL (MAV, LOWPASS)
    
	return;
}


fp_value actuator_joint_overstretch_protection(fp_value motorposition, const fp_value MAX_ANGLE, const fp_value MIN_ANGLE)
{
	if (motorposition > MAX_ANGLE)
		return MAX_ANGLE;
	else if (motorposition < MIN_ANGLE)
		return MIN_ANGLE;
	else return motorposition;
}

fp_value maximal_deflection_length_security(const fp_value roll_position, const fp_value lever_position, const float HOTPOT_knee, const bool right_leg)
{
	//see MACCEPA paper:

	// Initialisation
	const float a0 = 0.231;
	const float b = 0.050;
	const float c = 0.281;
	float alpha = 0;
	const float MAX_DEFLECTION = 0.035;
    const float MIN_DEFLECTION = -0.002;
	const float	RADIUS_ROLL = 0.03;

	// Calculation
	alpha = HOTPOT_knee - lever_position;
	float deflection = sqrt(b*b + c*c - 2 * b*c*cos(alpha)) - a0 + roll_position * RADIUS_ROLL;

	// Safety test
	if (deflection > MAX_DEFLECTION)
	{
		if (right_leg) 
		{
		    cout<<"Right max. deflection length violated!\n";
		}
		else
		{
		    cout<<"Left max. deflection length violated!\n";
        }
		return roll_position - (deflection - MAX_DEFLECTION) / RADIUS_ROLL;
	}
	else if (deflection < MIN_DEFLECTION)
	    {
		    if (right_leg) 
		    {
		        cout<<"Right min. deflection length violated!\n";
		    }
		    else
		    {
		        cout<<"Left min. deflection length violated!\n";
            }
		    return roll_position - (deflection - MIN_DEFLECTION) / RADIUS_ROLL;
	    }
        else
	    {
	        return roll_position;
        }
}

fp_value knee_security_check(const fp_value lever_motorposition, const float HOTPOT_lever, const float HOTPOT_knee, const bool right_knee, const fp_value MIN_KNEE, const fp_value MAX_KNEE)
{
	//check if knee_angle is violated

	if (HOTPOT_knee > MAX_KNEE)
		if (lever_motorposition > 0) //HAS TO BE CHECKED AFTER DISCUSSION WITH ELECTRICAL ENGINEERS
		{
			if (right_knee) cout << "Right knee was overstretched\n";
			else cout << "Left knee was overstretched\n";
			return MAX_KNEE - HOTPOT_knee;
		}
		else return lever_motorposition;
	else return lever_motorposition;
}




bool safe_state_call(const sensor_processing & sp, const Sensor_Datenpacket & sd, const bool slave4safe)
{

	const float MAX_TEMP_MCU = 70;			//[°C]
	const float MAX_TEMP_UPPER_LEG = 70;	//[°C]
	const float MAX_TEMP_BATT_BOX = 70;		//[°C]

	const float MAX_BATT_LEVEL = 45;		//[V]
	const float MIN_BATT_LEVEL = 35;		//[V]


	// Call by slave or button
	if (slave4safe)
	{
		state = safe;
		state_prev = safe;
		cout << "Safe state due to WISH OF SLAVE! \n";
		return true;
	}

	//CHANGE THIS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	const float SD_BATTERY_LEVEL = 37;
	const float SD_TEMP_RIGHT_LEG = 25;
	const float SD_TEMP_LEFT_LEG = 25;
	const float SD_TEMP_BATT_BOX = 30;
	//CHANGE PREVIOUS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// battery level to low
	if (SD_BATTERY_LEVEL > MAX_BATT_LEVEL)
	{
		state = safe;
		state_prev = safe;
		cout << "Safe state due to TO HIGH BATTERY LEVEL" << SD_BATTERY_LEVEL << "V! \n";
		return true;
	}
	if (SD_BATTERY_LEVEL < MIN_BATT_LEVEL)
	{
		state = safe;
		state_prev = safe;
		cout << "Safe state due to TO LOW BATTERY LEVEL" << SD_BATTERY_LEVEL << "V! \n";
		return true;
	}

	// temperature somewhere to high 
		//check temperature for every MCU
		//check temperature in battery box
		//check temperature in upper leg
	for (int i = 0; i < 6; ++i)
		if (sd.MOTORENWERTE[i][1] > MAX_TEMP_MCU)
		{
			state = safe;
			state_prev = safe;
			cout << "Safe state due to TO HIGH TEMPERATURE " << sd.MOTORENWERTE[i][1] << "°C AT MCU N°" << i << " !\n";
			return true;
		};
	if (SD_TEMP_RIGHT_LEG > MAX_TEMP_UPPER_LEG)
	{
		state = safe;
		state_prev = safe;
		cout << "Safe state due to TO HIGH TEMPERATURE " << SD_TEMP_RIGHT_LEG << "°C IN RIGHT UPPER LEG!\n";
		return true;
	}
	if (SD_TEMP_LEFT_LEG > MAX_TEMP_UPPER_LEG)
	{
		state = safe;
		state_prev = safe;
		cout << "Safe state due to TO HIGH TEMPERATURE " << SD_TEMP_LEFT_LEG << "°C IN LEFT UPPER LEG!\n";
		return true;
	}
	if (SD_TEMP_BATT_BOX > MAX_TEMP_BATT_BOX)
	{
		state = safe;
		state_prev = safe;
		cout << "Safe state due to TO HIGH TEMPERATURE " << SD_TEMP_BATT_BOX << "°C IN BATTERY BOX!\n";
		return true;
	}

	// spring jams
		// check if motor input has a correlation with motor output
		// ex. if motor should turn at 10 rpm check if motor turns at all
		// has to be implemented on slave --> transmission of a bool by slave
		//NOT IMPLEMENTED YET --> CAN BE IMPLEMENTED

	// rope tension drops
		// same check as for spring jams
		// if there is no corelation between motor input and motor ouput then --> safe state
		// --> slave
		//NOT IMPLEMENTED YET --> CAN BE IMPLEMENTED

	return false;
}

// end Marc's function definition
