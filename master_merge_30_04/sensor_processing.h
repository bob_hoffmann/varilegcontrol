// put in varileginterface

#ifndef SENSOR_PROCESSING_HEADER
#define SENSOR_PROCESSING_HEADER

#include <vector>
#include "spline.h"

using namespace std;

struct ground_status
{
	bool onGround_right = true;
	bool onGround_left = true;
	/*
		float max_GRF = 0;
		int id_max_GRF[2] = {0};
	*/
};

class sensor_processing
{

private:

    // "input variables"
	//vector<vector<float>> MOTORENWERTE;
	vector<vector<vector<float>>> IMU, LOADCELL;		//Marc's comment: First element is for the elapsed values
    vector<vector<float>> HOTPOT, RPM;					//Marc's comment: First element is for the elapsed values
															// Marc Commentar: I would have named it SOFTPOT
															// Marc Commentar: Is a HipPoti vector member of this class necessary? Or will we treat them same as "HotPot"?
															// Marc Commentar: Perhaps it is necessary to distinguish between HotPot_sparkfun and HotPot_sensofoil, because of filtering reasons?

    // overloaded filter
	void dt_filter(vector<vector<vector<float>>> & data, vector<vector<vector<float>>> & filtered_data, const int parameter = 0);
	void dt_filter(vector<vector<float>> & data, vector<vector<float>> & filtered_data, const int parameter = 0);
    
    // converting sensor data to something usefull
    void loadcell_2_GRF();									// Marc Commentar: Why don't we let process the slave if GRF is there or not? Please refer also to the cpp file
    void sensor_fusion();

	// Define here how many old values are necessary

	const unsigned int MEMORYSIZE = 3;

	// Memory filtered variables
	vector<vector<vector<float>>> loadcell_old, filtered_IMU_old;
	vector<vector<float>> joint_phi_old, RPM_old;

public:

    // "output variables"
    // public variables for easier access
    ground_status GRF;
    vector<float> joint_phi, /*absolute_psi,*/ dphi, ddphi;
    
    // Constructor
    sensor_processing(const vector<vector<float>> & imu, const vector<vector<float>> & loadcell, const vector<vector<float>> & motorenwerte, const vector<float> & hotpot);

	// Process the sensor data
	void process(const vector<vector<float>> & imu, const vector<vector<float>> & loadcell, const vector<vector<float>> & motorenwerte, const vector<float> & hotpot);
};

#endif
