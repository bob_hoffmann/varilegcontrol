#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "position_controller.h"


/*	===============================================
* 	Set PWM speed according to the following rules:
* 	820  --> max reverse speed (CCW)
* 	4096 --> zero speed (motor forces to stand still)
* 	7372 --> max forward speed (CW)
* 	===============================================
*/

//Master_Data struct

//Master_Data struct


#include "own_libs/defines.h"
//

#define pi 3.14159265359


////// PID Controller //////
// Errors for MCU1 and MCU2
float previous_error_1 = 0, previous_error_2 = 0;
float integral_1 = 0, integral_2 = 0;
float error_1 = 0, errror_2 = 0;
float derivative_1 = 0, derivative_2 = 0;
float PID_output_1 = 0, PID_output_2 = 0;
int motor_position_int_old_1 = -1, motor_position_int_old_2 = -1;

////// Hardware ///////
const int GEAR_RATIO = 161;

////// Data transfer //////
// new_packet
uint8_t new_packet = 0;
uint8_t counter_old = 13;
uint8_t iterator = 0;

// mapping parameters master slave
const float OUT_MIN_MS = -pi;
const float OUT_MAX_MS = pi;
const float IN_MIN_MS = 0;
const float IN_MAX_MS = 65536;

// motor constants
const float MAX_MOTOR_RPM = 3500; // [rpm]
const float MAX_MOTOR_RAD_S = 3500. / 60. * 2 * pi; // [rad/s]
const float CONVERSION_COEFF = 60. / (2.*pi); // [RPM 1/(rad/s) ] v_rpm = CONVERSION_COEFF * v_rad_s

// mapping parameters pwm
const float OUT_MIN_PWM = 820;
const float OUT_MAX_PWM = 7372;
const float IN_MIN_PWM = -3500. / 60.*2.*pi;
const float IN_MAX_PWM = 3500. / 60.*2.*pi;

// constants related to data transfer
const int POS_FREQUENCY = POS_CONTROLL_FREQ;
const float DT = 1. / POS_FREQUENCY;


const int POS_VALUES = POS_CONTROLL_VAL_COUNT;

/* /Testing/ */
/*
const uint16_t SLOW_POS_VALUES = 62;//63;						// Marc's comment: WHY SOMETIMES uint16_t and sometimes uint8_t?
const uint16_t SLOW_LOOP_VALUE = 10;
int16_t slow_loop = 0;										// Marc's comment: What is the difference between uint and int?
int16_t slow_iterator = 0;
// 10°*sin() + 10°
int POS[] = {36409,36772,37132,37485,37827,38154,38465,38754,39021,39261,39473,39654,39802,39917,39997,40041,40048,40019,39955,39854,39720,39552,39353,39124,38868,38588,38286,37965,37629,37280,36923,36560,36196,35835,35478,35132,34798,34480,34181,33905,33653,33430,33236,33073,32944,32850,32791,32768,32782,32832,32918,33038,33192,33379,33595,33840,34111,34404,34717,35048,35392,35746,36106};
//int POS[] = {40050,40777,41496,42202,42885,43541,44161,44741,45273,45754,46177,46539,46837,47066,47226,47313,47328,47271,47141,46941,46671,46335,45937,45480,44968,44408,43804,43162,42489,41792,41077,40353,39625,38901,38189,37495,36827,36192,35594,35042,34539,34091,33703,33378,33120,32932,32814,32769,32796,32896,33067,33308,33617,33989,34423,34912,35453,36040,36667,37327,38015,38723,39445};
*/
/* /Testing/ */


uint16_t position_controll_mcu(Master_Data *master_data, Slave_Data *slave_data, int mcu_origin)
{
	/* /Testing/ */
	/*
	master_data->COUNTER = 0;

	slow_loop--;


	iterator = 0;
	if(slow_loop <= 0)
	{
	slow_loop = SLOW_LOOP_VALUE;
	slow_iterator++;
	if(slow_iterator>=SLOW_POS_VALUES)
	{
	slow_iterator = 0;
	}
	master_data->MCU1_POS[0] = POS[slow_iterator];
	master_data->MCU1_POS[1] = POS[slow_iterator+1];
	master_data->MCU1_POS[2] = POS[slow_iterator+2];
	master_data->MCU1_POS[3] = POS[slow_iterator+3];
	master_data->MCU1_POS[4] = POS[slow_iterator+4];
	//dt = 1./POS_FREQUENCY; // eventually adjust depending on SLOW_LOOP_VALUE
	}
	//else
	//{
	//    slow_iterator = 0;			// Marc's comment: What's the idea?
	//}
	*/
	/* /Testing/ */


	// differentiating slaves and mcus
	float joint_angle;
	uint16_t joint_angle_raw;
	int8_t motor_direction;
	uint16_t MCU_POS[POS_CONTROLL_VAL_COUNT];
	////// Safety Parameters //////
	// Joint
	// on raw data !!!
	// Final Stop
	float MIN_MCU_1;
	float MIN_MCU_2;
	float MAX_MCU_1;
	float MAX_MCU_2;
	// Virtual Stop
	float VIRTUAL_MIN_MCU_1;
	float VIRTUAL_MIN_MCU_2;
	float VIRTUAL_MAX_MCU_1;
	float VIRTUAL_MAX_MCU_2;

	// check if new packet
	if (counter_old != master_data->COUNTER)
		iterator = 0;
	else if (iterator + 2<POS_VALUES - 1) iterator++;// check bounds to avoid out of range array access during vcnt calculation			
		 else iterator = POS_VALUES - 2; // master has to consider that last value is not used by the slave
	// end check if new packet
	
	uint16_t new_pwm_speed = 0;

	switch (slave_data->ID)
	{
	case 1:		
			MIN_MCU_1 = LEVER_LEFT_MIN; MIN_MCU_2 = WINCH_LEFT_MIN; MAX_MCU_1 = LEVER_LEFT_MAX; MAX_MCU_2 = WINCH_LEFT_MAX;										// Set the mechanical endstops 
			VIRTUAL_MIN_MCU_1 = MIN_MCU_1 + 50; VIRTUAL_MIN_MCU_2 = MIN_MCU_2 + 50; VIRTUAL_MAX_MCU_1 = MAX_MCU_1 - 50; VIRTUAL_MAX_MCU_2 = MAX_MCU_2 - 50;		// Set the virtual endstops 
			
			// Call the standard PID controller with variables for our problem

			//MCU 1
			joint_angle = LeftKneePot2Rad(slave_data->SOFT_POT_1);
			motor_direction = 1; // "-1" for right side; left "1"
			new_pwm_speed = PID_control_MCU(KP_11, KI_11, KD_11, K_marius_11, &previous_error_1, &motor_position_int_old_1, master_data->MCU1_POS, joint_angle, slave_data->SOFT_POT_1, motor_direction, iterator, MIN_MCU_1, MAX_MCU_1, VIRTUAL_MIN_MCU_1, VIRTUAL_MAX_MCU_1);
			
			
			//MCU 2
			joint_angle = LeftWinchPot2Rad(slave_data->SOFT_POT_2);
			motor_direction = 1; // "-1" for right side; left "1"
			new_pwm_speed = PID_control_MCU(KP_12, KI_12, KD_12, K_marius_12, &previous_error_2, &motor_position_int_old_2, master_data->MCU2_POS, joint_angle, slave_data->SOFT_POT_2, motor_direction, iterator, MIN_MCU_2, MAX_MCU_2, VIRTUAL_MIN_MCU_2, VIRTUAL_MAX_MCU_2);

			break;
	case 2:
			MIN_MCU_1 = HIP_LEFT_MIN; MIN_MCU_2 = HIP_RIGHT_MIN; MAX_MCU_1 = HIP_LEFT_MAX; MAX_MCU_2 = HIP_RIGHT_MAX;											// Set the mechanical endstops 
			VIRTUAL_MIN_MCU_1 = MIN_MCU_1 + 300; VIRTUAL_MIN_MCU_2 = MIN_MCU_2 + 50; VIRTUAL_MAX_MCU_1 = MAX_MCU_1 - 800; VIRTUAL_MAX_MCU_2 = MAX_MCU_2 - 50;	// Set the virtual endstops 

			// Call the standard PID controller with variables for our problem

			//MCU 1
			joint_angle = LeftHipPot2Rad(slave_data->HIP_POT_1);
			motor_direction = 1; // "-1" for right side; left "1"
			new_pwm_speed = PID_control_MCU(KP_21, KI_21, KD_21, K_marius_21, &previous_error_1, &motor_position_int_old_1, master_data->MCU1_POS, joint_angle, slave_data->SOFT_POT_1, motor_direction, iterator, MIN_MCU_1, MAX_MCU_1, VIRTUAL_MIN_MCU_1, VIRTUAL_MAX_MCU_1);


			//MCU 2
			joint_angle = LeftHipPot2Rad(slave_data->HIP_POT_2);
			motor_direction = -1; // "-1" for right side; left "1"
			new_pwm_speed = PID_control_MCU(KP_22, KI_22, KD_22, K_marius_22, &previous_error_2, &motor_position_int_old_2, master_data->MCU2_POS, joint_angle, slave_data->SOFT_POT_2, motor_direction, iterator, MIN_MCU_2, MAX_MCU_2, VIRTUAL_MIN_MCU_2, VIRTUAL_MAX_MCU_2);

			break;
	case 3:
			MIN_MCU_1 = LEVER_RIGHT_MIN; MIN_MCU_2 = WINCH_RIGHT_MIN; MAX_MCU_1 = LEVER_RIGHT_MAX; MAX_MCU_2 = WINCH_RIGHT_MAX;									// Set the mechanical endstops 
			VIRTUAL_MIN_MCU_1 = MIN_MCU_1 + 50; VIRTUAL_MIN_MCU_2 = MIN_MCU_2 + 50; VIRTUAL_MAX_MCU_1 = MAX_MCU_1 - 50; VIRTUAL_MAX_MCU_2 = MAX_MCU_2 - 50;		// Set the virtual endstops 
			
			// Call the standard PID controller with variables for our problem

			//MCU 1
			joint_angle = SOFT_POT_TO_RAD(slave_data->SOFT_POT_1);
			motor_direction = -1; // "-1" for right side; left "1"
			new_pwm_speed = PID_control_MCU(KP_31, KI_31, KD_31, K_marius_31, &previous_error_1, &motor_position_int_old_1, master_data->MCU1_POS, joint_angle, slave_data->SOFT_POT_1, motor_direction, iterator, MIN_MCU_1, MAX_MCU_1, VIRTUAL_MIN_MCU_1, VIRTUAL_MAX_MCU_1);


			//MCU 2
			joint_angle = SOFT_POT_TO_RAD(slave_data->SOFT_POT_2);
			motor_direction = -1; // "-1" for right side; left "1"
			new_pwm_speed = PID_control_MCU(KP_32, KI_32, KD_32, K_marius_32, &previous_error_2, &motor_position_int_old_2, master_data->MCU2_POS, joint_angle, slave_data->SOFT_POT_2, motor_direction, iterator, MIN_MCU_2, MAX_MCU_2, VIRTUAL_MIN_MCU_2, VIRTUAL_MAX_MCU_2);
			
			break;
	}
	// Packet counter
	counter_old = master_data->COUNTER;
	return new_pwm_speed;
}



uint16_t PID_control_MCU(float KP, float KI, float KD, float K_marius, float* previous_error, int* motor_position_int_old, const uint16_t MCU_POS[POS_CONTROLL_VAL_COUNT], float joint_angle, const uint16_t joint_angle_raw, const int8_t motor_direction, const  uint8_t iterator, const float MIN_MCU, const float MAX_MCU, const float VIRTUAL_MIN_MCU, const float VIRTUAL_MAX_MCU)
{
	// Initialisation
	uint16_t new_pwm_speed = 0;
	float new_motor_speed = 0;

	// access motor value
	int motor_position_int = MCU_POS[iterator];
	int motor_position_int_1 = MCU_POS[iterator + 1];

	// inverse mapping as "map" in control_functions.cpp
	float motor_position = (motor_position_int - IN_MIN_MS) * (OUT_MAX_MS - OUT_MIN_MS) / (IN_MAX_MS - IN_MIN_MS) + OUT_MIN_MS; // [rad]
	float motor_position_1 = (motor_position_int_1 - IN_MIN_MS) * (OUT_MAX_MS - OUT_MIN_MS) / (IN_MAX_MS - IN_MIN_MS) + OUT_MIN_MS; // [rad]


	////// Safety 1. ///////

	// Check joint angles
	/*if(joint_angle_raw<VIRTUAL_MIN_MCU_1)
	{
	if(motor_position<SOFT_POT_TO_RAD(VIRTUAL_MIN_MCU_1))
	{
	motor_position = SOFT_POT_TO_RAD(VIRTUAL_MIN_MCU_1);
	motor_position_1 = SOFT_POT_TO_RAD(VIRTUAL_MIN_MCU_1);
	}
	}
	if(joint_angle_raw>virtual_MAX_MCU_1)
	{
	if(motor_position>SOFT_POT_TO_RAD(virtual_MAX_MCU_1))
	{
	motor_position = SOFT_POT_TO_RAD(virtual_MAX_MCU_1);
	motor_position_1 = SOFT_POT_TO_RAD(virtual_MAX_MCU_1);
	}
	}*/


	////// PID Controller ///////

	// calculate proportional error
	error = motor_position - joint_angle;

	// calculate derivative error
	derivative = (error - *previous_error) * POS_FREQUENCY;
	
	/* // for hip and pretension:
	* if(motor_position_int_old != -1)
	* {
	* 		derivative = (motor_position_int -motor_position_int_old)/dt - mcu1_rpm;
	* }
	* else
	* {
	* 		derivative = 0;
	* }
	*/

	// calculate integrale error
	integral = integral + error*DT;

	// Sum PID errors
	PID_output = KP*error + KI*integral + KD*derivative;

	// end PID Controller

	// velocity feedforward
	float vcnt = (motor_position_1 - motor_position) * POS_FREQUENCY;

	/* Testing */
	// PID Parameters
	// vcnt = 0;
	/* Testing */

	// sum PID and velocity feedforward
	new_motor_speed = PID_output + K_marius*vcnt;


	// gear transmisson ratio
	new_motor_speed = motor_direction * new_motor_speed * GEAR_RATIO; // "-new_motor_speed" for right side; left "new_motor_speed"

	////// Safety 2. ///////
	// Check motor speed
	if (new_motor_speed > MAX_MOTOR_RAD_S)
		new_motor_speed = MAX_MOTOR_RAD_S;
	if (new_motor_speed < -MAX_MOTOR_RAD_S)
		new_motor_speed = -MAX_MOTOR_RAD_S;





	// tranformation rad/s to pwm
	new_pwm_speed = (int)((new_motor_speed - IN_MIN_PWM) * (OUT_MAX_PWM - OUT_MIN_PWM) / (IN_MAX_PWM - IN_MIN_PWM) + OUT_MIN_PWM);

	// store old values
	
	// PID controller
	*previous_error = error;
	
	// Reference position
	*motor_position_int_old = motor_position_int;

	// a posteriori anti-windup
	if (new_motor_speed >= MAX_MOTOR_RAD_S)													// Marc's comment: What is the idea behind?
		integral = integral - error*DT;


	////// Safety 3. ///////

	// Check joint angles

	if (joint_angle_raw < VIRTUAL_MIN_MCU_1)
		new_pwm_speed = 4096;
	if (joint_angle_raw > VIRTUAL_MAX_MCU_1)
		new_pwm_speed = 4096;
	if (joint_angle_raw < MIN_MCU_1)
		new_pwm_speed = 4096;
	if (joint_angle_raw > MAX_MCU_1)
		new_pwm_speed = 4096;
	// Check motor speed
	if (new_pwm_speed > 7372)
		new_pwm_speed = 7372;
	if (new_pwm_speed < 820)
		new_pwm_speed = 820;


	// new_pwm_speed = 5000;
	return new_pwm_speed;
}