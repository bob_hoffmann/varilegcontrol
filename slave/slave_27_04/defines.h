#ifndef __POSITION_CONTROLLER_H__
#define __POSITION_CONTROLLER_H__

#include "data_interface.h"

/*
 * Assumption:
 * MCU: -4000 rpm -> 0V
 * 		4000  rpm -> 3V
 *
 * transfer function:	-418 + (ADC_VALUE / 4096) * 836
 */

#define MCU_ADC_TO_RAD(x) (-418 + ((x)/4096.)*836)
#define SOFT_POT_TO_RAD(x) (((x - 1175.)/4096.)*0.9*3.142)
#define LeftHipPot2Rad(x) ((3.142/3)/(2897-1174)*(x-1174))
#define LeftWinchPot2Rad(x) (0.02/(3184-2478)*(x-2478)/0.03)
#define LeftLeverPot2Rad(x) (3.142/2/(2845-1634)*(x-1634))
#define LeftKneePot2Rad(x) (3.142/2/(2606-1620)*(x-1620))

//global variables
float PC_MCU1_P = 0.0;
float PC_MCU1_I = 0.0;
float PC_MCU1_D = 0.0;

float PC_MCU2_P = 0.0;
float PC_MCU2_I = 0.0;
float PC_MCU2_D = 0.0;

float PC_MCU3_P = 0.0;
float PC_MCU3_I = 0.0;
float PC_MCU3_D = 0.0;



uint16_t position_controll_mcu(Master_Data *master_data, Slave_Data *slave_data, int mcu_origin);

#endif

