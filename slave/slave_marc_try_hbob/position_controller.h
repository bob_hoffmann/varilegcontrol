#ifndef __POSITION_CONTROLLER_H__
#define __POSITION_CONTROLLER_H__

#include "data_interface.h"

/*
 * Assumption:
 * MCU: -4000 rpm -> 0V
 * 		4000  rpm -> 3V
 *
 * transfer function:	-418 + (ADC_VALUE / 4096) * 836
 */

#define MCU_ADC_TO_RAD(x) (-418 + ((x)/4096.)*836.)
#define SOFT_POT_TO_RAD(x) (((x - 1175.)/4096.)*0.9*3.142)
#define LeftHipPot2Rad(x) ((3.142/3.)/(2897.-1174.)*(x-1174.))
#define LeftWinchPot2Rad(x) (0.02/(3184.-2478.)*(x-2478.)/0.03)
#define LeftLeverPot2Rad(x) (3.142/2./(2845.-1634.)*(x-1634.))
#define LeftKneePot2Rad(x) (3.142/2./(2606.-1620.)*(x-1620.))

//global variables
const float KP_11 = 2;
const float KI_11 = 0;
const float KD_11 = 0;
const float K_marius_11 = 0;

const float KP_12 = 1.;
const float KI_12 = 0.001;
const float KD_12 = 0;
const float K_marius_12 = 0;

const float KP_21 = 1.;
const float KI_21 = 0;
const float KD_21 = 0;
const float K_marius_21 = 0;

const float KP_22 = 1;
const float KI_22 = 0;
const float KD_22 = 0;
const float K_marius_22 = 0;

const float KP_31 = 0;
const float KI_31 = 0;
const float KD_31 = 0;
const float K_marius_31 = 0;

const float KP_32 = 0;
const float KI_32 = 0;
const float KD_32 = 0;
const float K_marius_32 = 0;

uint16_t position_controll_mcu(Master_Data *master_data, Slave_Data *slave_data, int mcu_origin);
uint16_t PID_control_MCU(float KP, float KI, float KD, float K_marius, float* previous_error, int* motor_position_int_old, const uint16_t MCU_POS[POS_CONTROLL_VAL_COUNT], float joint_angle, const uint16_t joint_angle_raw, const int8_t motor_direction, const  uint8_t iterator, const float MIN_MCU, const float MAX_MCU, const float VIRTUAL_MIN_MCU, const float VIRTUAL_MAX_MCU);

#endif

