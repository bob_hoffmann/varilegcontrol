#include "Packet_Reciever.h"
#include "IDatenPacket.h"
#include "Intercom_datenpacket.h"
#include "Output_Datenpacket.h"
#include "Sensor_Datenpacket.h"
#include "Commands_Datenpacket.h"
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <zmq.hpp>

using namespace std;



zmq::context_t *context;
zmq::socket_t *socket;


int main(int argc, const char* argv[])
{

	Packet_Reciever rec;
	
	

	Intercom_datenpacket *icd=new Intercom_datenpacket(6,5);
	Intercom_datenpacket *icd2 = new Intercom_datenpacket();
	icd->data[1] = 70;
	*icd2 = rec.reciveIntercom(icd->convertToByte());
	std::cout << "ict->data[1]:  " << icd->data[1] << "\n";
	std::cout << "ict2->data[1]:  " << icd2->data[1] << "\n";
	std::cout << "ict.time:  " << icd->time << "\n";
	std::cout << "ict2.time:  " << icd2->time << "\n";
	std::cout <<  "\n";



	Sensor_Datenpacket *sd=new Sensor_Datenpacket(1);
	Sensor_Datenpacket *sd2 = new Sensor_Datenpacket();
	sd->IMU[2][3]=6;
	*sd2 = rec.reciveSensor(sd->convertToByte());
	std::cout << "sd->IMU[2][3]:  " << sd->IMU[2][3] << "\n";
	std::cout << "sd2->IMU[2][3]:  " << sd2->IMU[2][3]<<"\n";
	std::cout << "sd->time:  " << sd->time << "\n";
	std::cout << "sd2->time:  " << sd2->time << "\n";
	std::cout <<  "\n";



	Output_Datenpacket *odp= new Output_Datenpacket(5);
	Output_Datenpacket *odp2 = new Output_Datenpacket();
	odp->tweeter =15;	
	*odp2 = rec.reciveOutput(odp->convertToByte());
	std::cout << "odp->tweeter:  " <<(int) odp->tweeter<< "\n";
	std::cout << "odp2->tweeter:  " <<(int) odp2->tweeter << "\n";
	std::cout << "odp->time:  " << odp->time << "\n";
	std::cout << "odp2->time:  " << odp2->time << "\n";
	std::cout <<  "\n";



	Commands_Datenpacket *cod=new Commands_Datenpacket(3);
	Commands_Datenpacket *cod2 = new Commands_Datenpacket();
	cod->motorposition[1][4]=51;
	*cod2 = rec.reciveComand(cod->convertToByte());
	std::cout << "cod->motorposition[1][4]:  " << cod->motorposition[1][4]<< "\n";
	std::cout << "cod2->motorposition[1][4]:  " << cod2->motorposition[1][4] << "\n";
	std::cout << "cod->time:  " << cod->time << "\n";
	std::cout << "cod2->time:  " << cod2->time << "\n";
	std::cout <<  "\n";


}

std::vector<unsigned char>  rec_msg(){

zmq::message_t request;
socket->recv(&request);
vector<unsigned char> msg(request.size());
memcpy((void *)msg.data(), request.data(), request.size());
return msg;

}


void send(std::vector<unsigned char>  msg){

	 zmq::message_t reply( msg.size() );
	 memcpy((void *)reply.data(), msg.data(), msg.size());
	 socket->send(reply);
	

}

 void init_zmq(){
	 //  Prepare our context and socket
	 context = new zmq::context_t(1);
	 socket = new zmq::socket_t(*context, ZMQ_PAIR);
	 socket->bind("tcp://*:5555");

 }
