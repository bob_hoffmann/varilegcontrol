

#include "IDatenPacket.h"
#include <vector>

unsigned long IDatenPacket::extractTime(std::vector<unsigned char> msg){

	unsigned long t = 0;
	t = msg[1];
	t += (((unsigned long)msg[2]) >> 8);
	t += (((unsigned long)msg[3]) >> 16);
	t += (((unsigned long)msg[4]) >> 24);
	return t;

}

std::vector<unsigned char>   IDatenPacket::createHeader(std::vector<unsigned char>  msg, unsigned long time_t, unsigned char ID_t)
{
	
	std::vector<unsigned char> packet(msg.size() + 5);

	

	

	packet[0] = ID_t;

	//Converts time to the array
	packet[1] = time_t & 0xff;
	packet[2] = (time_t >> 8) & 0xff;
	packet[3] = (time_t >> 16) & 0xff;
	packet[4] = (time_t >> 24) & 0xff;


	for (unsigned int i = 0; i < msg.size(); i++)
	{
		packet[i + 5] = msg[i];
	}


	return packet;

}


std::vector<unsigned char>   IDatenPacket::detachHeader(std::vector<unsigned char>  msg)
{
	

	std::vector<unsigned char>  packet (msg.size() - 5);
	for (unsigned int i = 0; i < msg.size() - 5; i++)
	{
		packet[i] = msg[i + 5];
	}

	return packet;
}


//Deallocatin of the vektor msg is not done. Not shure if neccessair and needed



//long IDatenPacket::getAnzahlBytes(){ return 0; }
//unsigned char IDatenPacket::getID(){ return 70; }
//std::vector<unsigned char> IDatenPacket::convertToByte(){ std::vector<unsigned char> abs(1); return abs; }
//void IDatenPacket::convertFromByte(std::vector<unsigned char>  packet){}
