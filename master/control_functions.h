#ifndef CONTROL_FUNCTIONS_H
#define CONTROL_FUNCTIONS_H

#include <vector>
#include "spline.h"
#include "sensor_processing.h"
#include "Sensor_Datenpacket.h"

using namespace std;
// Bob's function prototypes

// initialize all global variables
void control_init();

// do spline interpolation at the start up
void spline_setup();
void spline_walk();
void spline_first_step();
void spline_last_step();

// finite state machine to decide in which state the exoskeleton is
int state_machine(vector<int> buttons,const ground_status & GRF, vector<fp_value> joint_phi, /* vector<fp_value> absolute_psi, */ int id_prev, int t_pseudo_right, int t_pseudo_left);

// advance time if exoskeleton is in a swing phase and pause time if exoskeleton is in a stand phase --> pseudo time depends on the state
long time_generator_left(long time_left_prev, long duration, int state);
long time_generator_right(long time_right_prev, long duration, int state);

// calculate the next few motor values using the pseudo time produced by the time generator
vector<vector<fp_value>> trajectory_generator(long pseudo_time_left, long pseudo_time_right, int state);

// calculate phi knee and phi hip knowing the x-y position of the foot and the angle of the torso
phi_inverse_kinematics inverse_kinematics(fp_value x_foot, fp_value y_foot, fp_value Phi_Torso); // Phi_Torso is constant for the moment

// As the motorvalues are converted from float to uint16 a conversion has to be applied.
// -pi - pi is mapped to 0-65536
vector<vector<int>> conversions(vector<vector<fp_value>> motor_positions); // iterate 2-D vector
int map(fp_value x, fp_value in_min = -M_PI, fp_value in_max=M_PI, fp_value out_min = 0, fp_value out_max = 65536); // mapping function


// end Bob's function prototypes

// Marc's function prototypes

// Security function has to check if maximal angles are not violated, if spring does not get to much deflected and if motorvalues make sense
void motor_security_check(vector<vector<fp_value>> & motorpositions, const sensor_processing & sp, const Sensor_Datenpacket & sd);

//Security function to check if angular extrema are violated and takes countermeasures
fp_value actuator_joint_overstretch_protection(const fp_value motorposition, const fp_value MAX_ANGLE, const fp_value MIN_ANGLE);

//compute the deflection length of the spring and check if it is not to long for 0..Motorendatenpacket --> release pretension roll
fp_value maximal_deflection_length_security(const fp_value roll_position, const fp_value lever_position, const float HOTPOT_knee, const bool right_leg);

// Security check for the knee in order to avoid overstretching of the knee joint
fp_value knee_security_check(const fp_value lever_motorposition, const float HOTPOT_lever, const float HOTPOT_knee, const bool right_knee, const fp_value MIN_KNEE, const fp_value MAX_KNEE);

// Function that calls safe state in case of emergency
void safe_state_call(const sensor_processing & sp, const Sensor_Datenpacket & sd, const bool slave4safe);


// end Marc's function prototypes
#endif
