#include "Packet_Reciever.h"
#include "IDatenPacket.h"
#include "Intercom_datenpacket.h"
#include "Output_Datenpacket.h"
#include "Sensor_Datenpacket.h"
#include "Commands_Datenpacket.h"
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <zmq.hpp>
#include <unistd.h>

// Bob's includes
#include <math.h>
#include "control_global_variables.h"
#include "sensor_processing.h"
#include "spline.h"
#include "control_functions.h"

using namespace std;


Packet_Reciever rec;
zmq::context_t *context;
zmq::socket_t *socket;


//global variable



std::vector<unsigned char>  rec_msg();
void send(std::vector<unsigned char>  msg);
void init_zmq();
bool first_receive = true;


void DecodeRecMsg(std::vector<unsigned char> msg);


void rec_msg(){

	zmq::message_t request;
	socket->recv(&request);
	vector<unsigned char> msg(request.size());
	memcpy((void *)msg.data(), request.data(), request.size());
	//std::cout <<(int) msg[0]<<endl;
	DecodeRecMsg(msg);
	return;

}


void send(std::vector<unsigned char>  msg){

	 zmq::message_t reply( msg.size() );
	 memcpy((void *)reply.data(), msg.data(), msg.size());
	 socket->send(reply);
	

}

void init_zmq(){

	 context = new zmq::context_t(1);
	 socket = new zmq::socket_t(*context, ZMQ_PAIR);
	 socket->connect("tcp://*:5555");

 }




/*
 * Code fuer BA und Controlling und so hier in die Event getriggerten Funktionen schreiben
*/



void OnInterRec(Intercom_datenpacket cd){
	
	cout<<"cd.data[1]: "<< cd.data[1]<<endl;

}

void OnComRec(Commands_Datenpacket cod){


}


void OnOutRec(Output_Datenpacket odp){

	cout<<(int)odp.tweeter<<endl;
}



void OnSensRec(Sensor_Datenpacket sd){				// Marc Commentar: I know that it has to be comment instead of commentar
	
	//!!!!!!!!!!!!!!!!!!ENTFERNEN UND IN INTERCOM MACHEN !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	const bool slave4safe = false;
	//!!!!!!!!!!!!!!!!!!!ENTFERNEN UND IN INTERCOM MACHEN !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	if(first_receive)										//Marc : Initialisation of the time
	{
		begin_time = std::chrono::steady_clock::now();
		first_receive = false;
	}
	//end time measurement							
    end_time= std::chrono::steady_clock::now();
	
    long duration = chrono::duration_cast<chrono::microseconds>(end_time - begin_time).count(); // [microseconds]
    //start time measurement
	begin_time = std::chrono::steady_clock::now();

    // Bob's sensor_processing
    sensor_processing sp(sd.IMU, sd.LOADCELL, sd.MOTORENWERTE, sd.HOTPOT);
        // Elements can now be accessed using sp.GRF , sp.dphi , etc.

	// Main safety 
	safe_state_call(sp, sd, slave4safe);

    // Bob's state machine
	state = state_machine(buttons, sp.GRF, sp.joint_phi,/* sp.absolute_psi, */ state_prev,pseudo_time_right,pseudo_time_left);
				

    // Bob's time
    virtual_time = virtual_time + duration;

    // Bob's time generator
    pseudo_time_left = time_generator_left(pseudo_time_left_prev, duration, state);
    pseudo_time_right = time_generator_right(pseudo_time_right_prev, duration, state);

    // Bob's Trajectory Generator
    vector<vector<fp_value>> calc_motorpositions = trajectory_generator(pseudo_time_left, pseudo_time_right, state);		
	motor_security_check(calc_motorpositions, sp, sd);
	vector<vector<int>> int_motorpositions = conversions(calc_motorpositions);

    // save old values; "feedback values"
    state_prev = state;

    // command package
	Commands_Datenpacket *cod=new Commands_Datenpacket(3);
    // cod->motorposition=int_motorpositions;

	send(cod->convertToByte());

}






int main(int argc, const char* argv[])
{
	//cout<<"Hello?"<<endl;
	init_zmq();
	first_receive = true;
    //begin_time = std::chrono::steady_clock::now();

	while(true){
		rec_msg();

		/* Code for sending
		Intercom_datenpacket *icd=new Intercom_datenpacket(6,5);
		send(icd->convertToByte());
		delete icd;
		*/
		

	}
	

}







void DecodeRecMsg(std::vector<unsigned char> msg){





	if(msg[0]==1){
		OnSensRec(rec.reciveSensor(msg));
	
	}
	if(msg[0]==2){
		OnOutRec(rec.reciveOutput(msg));

	}

	if(msg[0]==3){
		OnComRec(rec.reciveComand(msg));

	}
	if(msg[0]==4){
		OnInterRec(rec.reciveIntercom(msg));

	
	}

}




