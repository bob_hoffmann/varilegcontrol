#ifndef CONTROL_GLOBAL_VARIABLES_H
#define CONTROL_GLOBAL_VARIABLES_H

#include <vector>
#include "spline.h"
#include <chrono>

using namespace std;


// Bob's global variables


// change to double for increased precision
typedef float fp_value;

// spline
// walk
const int N_points = 13;
extern std::vector<fp_value> X, Y, T;
tk::spline sx,sy;
// first step
const int N_points_6 = 5;
extern std::vector<fp_value> X6,Y6,T6;
tk::spline sx6,sy6;
// last step
const int N_points_7 = 5;
extern std::vector<fp_value> X7,Y7,T7;
tk::spline sx7,sy7;


const double x0_foot = 0;
const double y0_foot = -1;

// inverse kinematics
struct phi_inverse_kinematics
{
    fp_value phi_knee,phi_hip;
};
const double Phi_Torso = 10./180.*M_PI;


// time related

extern std::chrono::steady_clock::time_point begin_time;
extern std::chrono::steady_clock::time_point end_time;

extern int state;						// Marc Commentar: I would propose to only use this global variable in every function to initialize constant (except for the state machine)
extern int state_prev;					// Marc Commentar: I would propose to only use this global variable in every function to initialize constant (except for the state machine)
extern long virtual_time;				// idem for the rest
extern long pseudo_time_left;
extern long pseudo_time_left_prev;
extern long pseudo_time_right;
extern long pseudo_time_right_prev;


const long T_Gait = 3000000; // [microseconds]

const int reference_pos_freq = 10; // [ms] before a new motor position is selected (in trajectory generator)		
const int number_motor_values = 5;


// thresholds

const fp_value GRF_THRES = 0;

// end Bob's global variables

// Marc's states variables

enum states{safe, stand_left_front, right_swing, stand_right_front, left_swing, stand_parallel, right_swing_begin, left_swing_end, left_swing_begin, right_swing_end};
extern vector<bool> buttons(3,0);	//Buttons for state machine	//bool can be changed into int if more than one mode exists for the buttons
									//0 : left ; 1 : right ; 2 : stand

// end Marc's part

#endif
