#include "control_global_variables.h"
// use extern keyword in header to avoid multiple definitions
#include <chrono>

using namespace std;

int state;
int state_prev;		
long virtual_time;
long pseudo_time_left;
long pseudo_time_left_prev;
long pseudo_time_right;
long pseudo_time_right_prev;
std::chrono::steady_clock::time_point begin_time;
std::chrono::steady_clock::time_point end_time;

std::vector<fp_value> X(N_points), Y(N_points), T(N_points);
std::vector<fp_value> X6(N_points_6),Y6(N_points_6),T6(N_points_6);
std::vector<fp_value> X7(N_points_7),Y7(N_points_7),T7(N_points_7);


