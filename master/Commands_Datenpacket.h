
#ifndef COM_HEADER
#define COM_HEADER

#include <vector>
#include "IDatenPacket.h"



class Commands_Datenpacket : IDatenPacket
{
public:
	//Anzahl werte f�r einen motor in einem packet
	int anzahl_motorenwerte=1000;

	//Gibt die Anzahl floats im Struckt an.
	int nummerOfFloats;

	//Die n�chsten motorenpositionen
	//Erste nummer ist f�r welchen motor
	//0 rechts hebel, 1 rechts rolle, 2 rechts h�fte, 3 links hebel, 4 links rolle, 5 links h�ft
	//Die zweite nummer ist die Position (zeitlich)
	vector<vector<float>>  motorposition;


	//Gibt an wievile flagbytes es gibt
	int anzahl_flagbytes;

	//Flag bytes
	 std::vector<unsigned char> flags;

	//Time in miliseconds of the datapacket
	 unsigned long time;

	 Commands_Datenpacket(unsigned long zeit);


	 Commands_Datenpacket();


	 void setTime(long ti);

	unsigned long getTime();

	 std::vector<unsigned char> convertToByte();



	 void convertFromByte(std::vector<unsigned char> msg);


	 int getAnzahlBytes();

	unsigned char getID();
};

#endif
