#include <cstdio>
#include <cstdlib>
#include <vector>
#include "spline.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>

/*
compile && plot:
$           g++ spline_foot_joint_4.cpp -o spline_foot_joint &&  ./spline_foot_joint && gnuplot
gnuplot>    set multiplot layout 2,1 rowsfirst; plot "xy_spline.dat" with lines, "xy.dat", "xy6.dat", "xy6_spline.dat" with lines,"xy7.dat", "xy7_spline.dat" with lines; plot "phi_knee.dat" with lines,"phi_hip.dat" with lines, "phi_knee_6.dat" with lines, "phi_hip_6.dat" with lines, "phi_knee_7.dat" with lines, "phi_hip_7.dat" with lines



*/

using namespace std;

int main(int argc, char** argv) {
    const double step_length=				0.5;// [m]
    const double step_height_begin=			0.23; // [m]
    const double step_height_end=			0.08; // [m]
    const double T_Gait= 					2.00; // [s]
    const double perc_time_on_floor=		0.5; // [-]	

    std::vector<double> X(13), Y(13), T(13),X6(5),Y6(5),T6(5),X7(5),Y7(5),T7(5);
    const double x0 = 0.0;							// [m]
    const double y0 =-1.0;							// [m]
/*    
    // swing phase begin && stance phase end
    X[1]= x0+0;
    X[2]= x0-0.1 * step_length;
    X[3]= x0+0;
    X[4]= x0+0.5 * step_length;
    X[5]= x0+0.95 * step_length;
    X[6]= x0+step_length;
    // swing phase end && stance phase begin
    X[7]= x0+0.8 * step_length;
    X[8]= x0+0.6 * step_length;
    X[9]= x0+0.5 * step_length;
    X[10]=x0+0.4 * step_length;
    X[11]=x0+0.3 * step_length;
    X[12]=x0+0.25 * step_length;
    X[13]=x0+0.18 * step_length;
    X[14]=x0+0.13 * step_length;
    X[15]=x0+0.05 * step_length;
    X[16]=X[1];
    X[17]=X[2];	
    X[0]= X[7];
*/
    // swing phase begin && stance phase end   
    X[1]= x0-0.22;
    X[2]= x0-0.30;
    X[3]= x0-0.02;
    X[4]= x0+0.35;
    X[5]= x0+0.51;
    X[6]= x0+0.52;
    //swing phase end && stance phase begin
    X[7]= x0+0.29;
    X[8]= x0+0.15;
    X[9]= x0+0.03;
    X[10]=x0-0.12;
    X[11]=X[1];
    // additional points to assure periodicity
    X[12]=X[2];
    X[0]= X[10];


    // swing phase begin && stance phase end
    Y[1]= y0+0.04;
    Y[2]= y0+0.08;
    Y[3]= y0+0.15;
    Y[4]= y0+0.10;
    Y[5]= y0+0.15;
    Y[6]= y0+0.11;
    // swing phase end && stance phase begin
    Y[7]= y0+0.05;
    Y[8]= y0+0.01;
    Y[9]= y0-0.02;
    Y[10]=y0+0;
    Y[11]= Y[1];
    // additional points to assure periodicity
    Y[12]= Y[2];
    Y[0]= Y[10];

    // swing phase begin && stance phase end
    T[1]=0;
    T[2]=0.1*T_Gait;
    T[3]=0.2*T_Gait;
    T[4]=0.3*T_Gait;
    T[5]=0.4*T_Gait;
    T[6]=0.5*T_Gait;
    // swing phase end && stance phase begin
    T[7]=0.6*T_Gait;
    T[8]=0.7*T_Gait;
    T[9]=0.8*T_Gait;
    T[10]=0.9*T_Gait;
    T[11]=T_Gait;
    // additional points to assure periodicity
    T[12]=T_Gait+T[2];
    T[0]=-0.1*T_Gait;

////////////////////////////////////////////////////////

    X6[0]= X[9];
    X6[1]= x0+0.10;
    X6[2]= X[4];
    X6[3]= X[5];
    X6[4]= X[6];

    Y6[0]= Y[9];
    Y6[1]= y0+0.05;
    Y6[2]= Y[4];
    Y6[3]= Y[5];
    Y6[4]= Y[6];


    T6[0]=0.8*T_Gait;
    T6[1]=T_Gait;
    T6[2]=T_Gait + 0.2*T_Gait;
    T6[3]=T_Gait + 0.4*T_Gait;    
    T6[4]=T_Gait + 0.5*T_Gait;
    
/////////////////////////////////////////////////////////    


    // swing phase begin && stance phase end   
    X7[0]= X[1];
    X7[1]= X[2];
    X7[2]= X[3];
    X7[3]= x0+0.1;
    X7[4]= X[9];


    // swing phase begin && stance phase end
    Y7[0]= Y[1];
    Y7[1]= Y[2];
    Y7[2]= Y[3];
    Y7[3]= y0+.08;
    Y7[4]= Y[9];
    

    // swing phase begin && stance phase end
    T7[0]=0;
    T7[1]=0.2*T_Gait;
    T7[2]=0.4*T_Gait;
    T7[3]=0.6*T_Gait;
    T7[4]=0.8*T_Gait;

////////////////////////////////////////////////////////    
    
    tk::spline sx,sy,sx6,sy6,sx7,sy7;
    sx.set_points(T,X);
    sy.set_points(T,Y);
    
    sx6.set_points(T6,X6);
    sy6.set_points(T6,Y6);
    
    sx7.set_points(T7,X7);
    sy7.set_points(T7,Y7);
    
        
    

    const char * filename = "xy.dat";
    ofstream outfile(filename);
    if (!outfile.is_open()) {
   		cout << "Could not open file " << filename << endl;
       	// abort
        return 1;
    }

    for(size_t i=0; i<X.size(); i++)
    {
	    outfile << X[i] << " " << Y[i] << endl;
	}
	outfile.close();

    const char * filename2 = "xy_spline.dat";
    ofstream outfile2(filename2);
    if (!outfile2.is_open()) {
   		cout << "Could not open file " << filename2 << endl;
       	// abort
        return 1;
    }

    for(int i=0; i<T_Gait*100; i++)
    {
        double t=0.01*i;
	    outfile2 << sx(t) << " " << sy(t) << endl;
    }
	outfile2.close();


    const char * filename3 = "phi_knee.dat";
    ofstream outfile3(filename3);
    if (!outfile3.is_open()) {
   		cout << "Could not open file " << filename3 << endl;
       	// abort
        return 1;
    }
    const char * filename4 = "phi_hip.dat";
    ofstream outfile4(filename4);
    if (!outfile4.is_open()) {
   		cout << "Could not open file " << filename4 << endl;
       	// abort
        return 1;
    }

    for(int i=0; i<200; i++)
    {
        double t=0.01*i;
        const double phi_torso = 0; 
        double x_foot = sx(t);
        double y_foot = sy(t);
	    double l2 = 0.48225;
        double l3 = 0.551225;
  
	    double c2 = (x_foot*x_foot + y_foot*y_foot - l2*l2 - l3*l3)/(2*l2*l3);
        if (c2*c2>1.)
        {
        	c2=1;
        }
        double s2 = sqrt(1 - c2*c2);
        double phi_knee = atan2(s2, c2); // theta2 is deduced
  
        double k1 = l2 + l3*c2;
        double k2 = l3*s2;
        double phi_hip =  (atan2(y_foot,x_foot) + atan2(k2, k1)) + phi_torso+M_PI/2;	
	    outfile3 << t << " " << phi_knee/M_PI*180 << endl;
        outfile4 << t << " " << phi_hip/M_PI*180 << endl;
    }
	outfile3.close();
    outfile4.close();
    


    const char * filename5 = "xy6.dat";
    ofstream outfile5(filename5);
    if (!outfile5.is_open()) {
   		cout << "Could not open file " << filename5 << endl;
       	// abort
        return 1;
    }

    for(size_t i=0; i<X6.size(); i++)
    {
	    outfile5 << X6[i] << " " << Y6[i] << endl;
	}
	outfile5.close();

    const char * filename6 = "xy6_spline.dat";
    ofstream outfile6(filename6);
    if (!outfile6.is_open()) {
   		cout << "Could not open file " << filename6 << endl;
       	// abort
        return 1;
    }

    for(float i=T_Gait*0.8; i<T_Gait+0.5*T_Gait; i=i+0.1)
    {
        double t=i;
	    outfile6 << sx6(t) << " " << sy6(t) << endl;
    }
	outfile6.close();

    const char * filename7 = "phi_knee_6.dat";
    ofstream outfile7(filename7);
    if (!outfile7.is_open()) {
   		cout << "Could not open file " << filename7 << endl;
       	// abort
        return 1;
    }
    const char * filename8 = "phi_hip_6.dat";
    ofstream outfile8(filename8);
    if (!outfile8.is_open()) {
   		cout << "Could not open file " << filename8 << endl;
       	// abort
        return 1;
    }

    for(float i=T_Gait*0.8; i<T_Gait+0.5*T_Gait; i=i+0.1)
    {
        double t=i;
        const double phi_torso = 0; 
        double x_foot = sx6(t);
        double y_foot = sy6(t);
	    double l2 = 0.48225;
        double l3 = 0.551225;
  
	    double c2 = (x_foot*x_foot + y_foot*y_foot - l2*l2 - l3*l3)/(2*l2*l3);
        if (c2*c2>1.)
        {
        	c2=1;
        }
        double s2 = sqrt(1 - c2*c2);
        double phi_knee = atan2(s2, c2); // theta2 is deduced
  
        double k1 = l2 + l3*c2;
        double k2 = l3*s2;
        double phi_hip =  (atan2(y_foot,x_foot) + atan2(k2, k1)) + phi_torso+M_PI/2;	
	    outfile7 << t << " " << phi_knee/M_PI*180 << endl;
        outfile8 << t << " " << phi_hip/M_PI*180 << endl;
    }
	outfile7.close();
    outfile8.close();
    
    const char * filename9 = "xy7.dat";
    ofstream outfile9(filename9);
    if (!outfile9.is_open()) {
   		cout << "Could not open file " << filename9 << endl;
       	// abort
        return 1;
    }

    for(size_t i=0; i<X7.size(); i++)
    {
	    outfile9 << X7[i] << " " << Y7[i] << endl;
	}
	outfile9.close();

    const char * filename10 = "xy7_spline.dat";
    ofstream outfile10(filename10);
    if (!outfile10.is_open()) {
   		cout << "Could not open file " << filename10 << endl;
       	// abort
        return 1;
    }

    for(float i=0; i<0.8*T_Gait; i=i+0.1)
    {
        double t=i;
	    outfile10 << sx7(t) << " " << sy7(t) << endl;
    }
	outfile7.close();

    const char * filename11 = "phi_knee_7.dat";
    ofstream outfile11(filename11);
    if (!outfile11.is_open()) {
   		cout << "Could not open file " << filename11 << endl;
       	// abort
        return 1;
    }
    const char * filename12 = "phi_hip_7.dat";
    ofstream outfile12(filename12);
    if (!outfile12.is_open()) {
   		cout << "Could not open file " << filename12 << endl;
       	// abort
        return 1;
    }

    for(float i=0; i<0.8*T_Gait; i=i+0.1)
    {
        double t=i;
        const double phi_torso = 0; 
        double x_foot = sx7(t);
        double y_foot = sy7(t);
	    double l2 = 0.48225;
        double l3 = 0.551225;
  
	    double c2 = (x_foot*x_foot + y_foot*y_foot - l2*l2 - l3*l3)/(2*l2*l3);
        if (c2*c2>1.)
        {
        	c2=1;
        }
        double s2 = sqrt(1 - c2*c2);
        double phi_knee = atan2(s2, c2); // theta2 is deduced
  
        double k1 = l2 + l3*c2;
        double k2 = l3*s2;
        double phi_hip =  (atan2(y_foot,x_foot) + atan2(k2, k1)) + phi_torso+M_PI/2;	
	    outfile11 << t << " " << phi_knee/M_PI*180 << endl;
        outfile12 << t << " " << phi_hip/M_PI*180 << endl;
    }
	outfile11.close();
    outfile12.close();
    

	
	return EXIT_SUCCESS;
}


