#include <cstdio>
#include <cstdlib>
#include <vector>
#include "spline.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>

/*
compile && plot:
$           g++ spline_foot_joint_3.cpp -o spline_foot_joint &&  ./spline_foot_joint && gnuplot
gnuplot>    set multiplot layout 2,1 rowsfirst; plot "xy_spline.dat", "xy.dat"; plot "phi_knee.dat","phi_hip.dat"

*/

using namespace std;

int main(int argc, char** argv) {
    const double step_length=				0.465;// [m]
    const double step_height_begin=			0.23; // [m]
    const double step_height_end=			0.12; // [m]
    const double T_Gait= 					2.00; // [s]
    const double perc_time_on_floor=		0.55; // [-]	

    std::vector<double> X(18), Y(18), T(18);
    const double x0 = 0;							// [m]
    const double y0 =-0.92;							// [m]
    
    // swing phase begin && stance phase end
    X[1]= x0+0;
    X[2]= x0-0.22 * step_length;
    X[3]= x0+0;
    X[4]= x0+0.5 * step_length;
    X[5]= x0+0.95 * step_length;
    X[6]= x0+step_length;
    // swing phase end && stance phase begin
    X[7]= x0+0.9 * step_length;
    X[8]= x0+0.8 * step_length;
    X[9]= x0+0.7 * step_length;
    X[10]=x0+0.6 * step_length;
    X[11]=x0+0.5 * step_length;
    X[12]=x0+0.4 * step_length;
    X[13]=x0+0.3 * step_length;
    X[14]=x0+0.2 * step_length;
    X[15]=x0+0.1 * step_length;
    X[16]=X[1];
    X[17]=X[2];	
    X[0]= X[7];


		// swing phase begin && stance phase end
    Y[1]= y0+0;
    Y[2]= y0+0.4 * step_height_begin;
    Y[3]= y0+step_height_begin;
    Y[4]= y0+0.6 * step_height_end;
    Y[5]= y0+step_height_end;
    Y[6]= y0+0;
    // swing phase end && stance phase begin
    Y[7]= y0+0;
    Y[8]= y0+0;
    Y[9]= y0+0;
    Y[10]=y0+0;
    Y[11]=y0+0;
    Y[12]=y0+0;
    Y[13]=y0+0;
    Y[14]=y0+0;
    Y[15]=y0+0;
    Y[16]= Y[1];
    Y[17]= Y[2];
    Y[0]= Y[9];

		// swing phase begin && stance phase end
    T[1]=0;																					
    T[2]=T[1]+0.2*(1-perc_time_on_floor)*T_Gait;
    T[3]=T[2]+0.2*(1-perc_time_on_floor)*T_Gait;
    T[4]=T[3]+0.2*(1-perc_time_on_floor)*T_Gait;
    T[5]=T[4]+0.2*(1-perc_time_on_floor)*T_Gait;
    T[6]=T[5]+0.2*(1-perc_time_on_floor)*T_Gait;			
    // swing phase end && stance phase begin
    T[7]=T[6]+0.10*perc_time_on_floor*T_Gait;
    T[8]=T[7]+0.10*perc_time_on_floor*T_Gait;
    T[9]=T[8]+0.10*perc_time_on_floor*T_Gait;
    T[10]=T[9]+0.10*perc_time_on_floor*T_Gait;
    T[11]=T[10]+0.10*perc_time_on_floor*T_Gait;
    T[12]=T[11]+0.10*perc_time_on_floor*T_Gait;
    T[13]=T[12]+0.10*perc_time_on_floor*T_Gait;
    T[14]=T[13]+0.10*perc_time_on_floor*T_Gait;
    T[15]=T[14]+0.10*perc_time_on_floor*T_Gait;
    T[16]=T_Gait;
    T[17]=T_Gait+T[2];
    T[0]=T[7]-T_Gait;


    tk::spline sx,sy;
    sx.set_points(T,X);
    sy.set_points(T,Y);
    

    const char * filename = "xy.dat";
    ofstream outfile(filename);
    if (!outfile.is_open()) {
   		cout << "Could not open file " << filename << endl;
       	// abort
        return 1;
    }

    for(size_t i=0; i<X.size(); i++)
    {
	    outfile << X[i] << " " << Y[i] << endl;
	}
	outfile.close();

    const char * filename2 = "xy_spline.dat";
    ofstream outfile2(filename2);
    if (!outfile2.is_open()) {
   		cout << "Could not open file " << filename2 << endl;
       	// abort
        return 1;
    }

    for(int i=0; i<T_Gait*100; i++)
    {
        double t=0.01*i;
	    outfile2 << sx(t) << " " << sy(t) << endl;
    }
	outfile2.close();


    const char * filename3 = "phi_knee.dat";
    ofstream outfile3(filename3);
    if (!outfile3.is_open()) {
   		cout << "Could not open file " << filename3 << endl;
       	// abort
        return 1;
    }
    const char * filename4 = "phi_hip.dat";
    ofstream outfile4(filename4);
    if (!outfile4.is_open()) {
   		cout << "Could not open file " << filename4 << endl;
       	// abort
        return 1;
    }

    for(int i=0; i<200; i++)
    {
        double t=0.01*i;
        const double phi_torso = -10*M_PI/180; 
        double x_foot = sx(t);
        double y_foot = sy(t);
	    double l2 = 0.48225;
        double l3 = 0.551225;
  
	    double c2 = (x_foot*x_foot + y_foot*y_foot - l2*l2 - l3*l3)/(2*l2*l3);
        if (c2*c2>1.)
        {
        	c2=1;
        }
        double s2 = sqrt(1 - c2*c2);
        double phi_knee = atan2(s2, c2); // theta2 is deduced
  
        double k1 = l2 + l3*c2;
        double k2 = l3*s2;
        double phi_hip =  (atan2(y_foot,x_foot) + atan2(k2, k1)) + phi_torso+M_PI/2;	
	    outfile3 << t << " " << phi_knee/M_PI*180 << endl;
        outfile4 << t << " " << phi_hip/M_PI*180 << endl;
    }
	outfile3.close();
    outfile4.close();
    
	
	return EXIT_SUCCESS;
}


