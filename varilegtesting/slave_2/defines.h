#ifndef __DEFINES_H__
#define __DEFINES_H__


//settings during testing

#define SLAVE_ID 1

#define IMU1_ON 0
#define IMU2_ON 1
#define IMU3_ON 1

#define SOFT_POT1_ON 1
#define SOFT_POT2_ON 0
#define SOFT_POT3_ON 0

#define HIP_POT_ON 1

#define POS_CONTROLL_ON 0
#define USB_COMM_ON 1



#define MCU_PWM_RESOLUTION 1 << 13;

#define MASTER_COMM_FREQ 20

#define POS_CONTROLL_FREQ 100
#define POS_CONTROLL_VAL_COUNT (int)( POS_CONTROLL_FREQ / MASTER_COMM_FREQ )


#endif
