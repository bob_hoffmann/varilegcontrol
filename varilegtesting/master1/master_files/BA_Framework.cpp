#include "Packet_Reciever.h"
#include "IDatenPacket.h"
#include "Intercom_datenpacket.h"
#include "Output_Datenpacket.h"
#include "Sensor_Datenpacket.h"
#include "Commands_Datenpacket.h"
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <zmq.hpp>
#include <unistd.h>


// Bob's includes
#include <math.h>
#include "control_f.h"


using namespace std;

Packet_Reciever rec;
zmq::context_t *context;
zmq::socket_t *socket;

//global variable
long virtual_time = 0;
auto begin_time = std::chrono::steady_clock::now();;
auto end_time = std::chrono::steady_clock::now();;



void DecodeRecMsg(std::vector<unsigned char> msg);


void rec_msg(){

	zmq::message_t request;
	socket->recv(&request);
	vector<unsigned char> msg(request.size());
	memcpy((void *)msg.data(), request.data(), request.size());
	//std::cout <<(int) msg[0]<<endl;
	DecodeRecMsg(msg);
	return;

}


void send(std::vector<unsigned char>  msg){

	 zmq::message_t reply( msg.size() );
	 memcpy((void *)reply.data(), msg.data(), msg.size());
	 socket->send(reply);
	

}

void init_zmq(){

	 context = new zmq::context_t(1);
	 socket = new zmq::socket_t(*context, ZMQ_PAIR);
	 socket->connect("tcp://*:5555");

 }







/*
 * Code fuer BA und Controlling und so hier in die Event getriggerten Funktionen schreiben
*/



void OnInterRec(Intercom_datenpacket cd){
	
	cout<<"cd.data[1]: "<< cd.data[1]<<endl;

}

void OnComRec(Commands_Datenpacket cod){


}


void OnOutRec(Output_Datenpacket odp){

	cout<<(int)odp.tweeter<<endl;
}


void OnSensRec(Sensor_Datenpacket sd){				// Marc's comment: GENERAL QUESTION: DON'T YOU NEED ANY GLOBAL VARIABLES?
	//end time measurement							
    end_time= std::chrono::steady_clock::now();
	
    long duration = chrono::duration_cast<chrono::microseconds>(end_time - begin_time).count(); // [microseconds]
    //start time measurement
	begin_time = std::chrono::steady_clock::now();

    // Bob's time
    virtual_time = virtual_time + duration;

    vector<vector<int>> motorposition_int(motors); //6 motors
    for(int i=0;i<motors;i++)
    {
        motorposition_int[i].resize(next_motor_values);				
    }
    
    motorposition_int = calc_trajectories(virtual_time,next_motor_values,update_t_slave);


    cout<<"Duration: "<<duration<<"\t virtual time "<<virtual_time<<endl;
    for(int i=3;i<6;i++)
    {
        for(int j=0;j<next_motor_values;j++)
        {
            cout<<"Motor "<<i<<" at Motorposition "<< j << ": " << motorposition_int[i][j] << "\n";		
            	
        }
    }

    // command package
	Commands_Datenpacket *cod=new Commands_Datenpacket(3);
    cod->motorposition=motorposition_int;

	send(cod->convertToByte());

}







int main(int argc, const char* argv[])
{
	//cout<<"Hello?"<<endl;
	init_zmq();
    begin_time = std::chrono::steady_clock::now();

	while(true){
		rec_msg();

		/* Code for sending
		Intercom_datenpacket *icd=new Intercom_datenpacket(6,5);
		send(icd->convertToByte());
		delete icd;
		*/
		

	}
	

}







void DecodeRecMsg(std::vector<unsigned char> msg){





	if(msg[0]==1){
		OnSensRec(rec.reciveSensor(msg));
	
	}
	if(msg[0]==2){
		OnOutRec(rec.reciveOutput(msg));

	}

	if(msg[0]==3){
		OnComRec(rec.reciveComand(msg));

	}
	if(msg[0]==4){
		OnInterRec(rec.reciveIntercom(msg));

	
	}

}





 




