#ifndef CONTROL_F_H
#define CONTROL_F_H

#include <vector>
#include <math.h>

using namespace std;

//global variables

const float pi = 3.141592653589793;
const float a_Hip_Angle[]={31.7855367367369,20.6337395029632,-2.35969874054236,-2.78287762626002,-1.58340571738558,-0.454786050351754,1.42674055911556,0.106234644432573,0.0533725601495364,-0.0681188702654617,0.204716223848431};
const float a_Knee_Angle[]={28.1397305620824,-0.351954031250211,-18.7664369619508,-14.3193431751729,5.65683340387221,-1.10179419209047,3.52751062664298,-0.478939425195133,0.379323919627380,-0.0588122653304773,0.427177610305164};
const long T_Gait = 10000000.; //[microseconds]
const float omega = 2.*pi;
const int next_motor_values = 5;
const int motors = 6;
const int update_t_slave = 10000; //[microseconds]

// Bob's function prototypes
float pretension(float t);
float knee_angle(float t);
float hip_angle(float t);
float grad_to_rad(float angle);
// As the motorvalues are converted from float to uint16 a conversion has to be applied.
// -pi - pi is mapped to 0-65536
vector<vector<int>> conversions(vector<vector<float>> motor_positions); // iterate 2-D vector
int map(float x, float in_min = -pi, float in_max=pi, float out_min = 0, float out_max = 65536); // mapping function
vector<vector<int>> calc_trajectories(float t_int,int next_values,long t_slave);

// Bob's function definitions
vector<vector<int>> calc_trajectories(long t_int,int next_values,long t_int_slave)
{
    vector<vector<float>> motorposition(motors); //6 motors
    vector<vector<int>> motorposition_int(motors); //6 motors
    for(int i=0;i<motors;i++)
    {
        motorposition[i].resize(next_values);
        motorposition_int[i].resize(next_values);
    }
    for(int i=0;i<next_values;i++)
    {
        float t_right = ((t_int+i*t_int_slave)%T_Gait)/float(T_Gait); // [microseconds to seconds]
        float t_left = ((t_int+int(T_Gait/2.)+i*t_int_slave)%T_Gait)/float(T_Gait); // [microseconds to seconds]
        cout<<"t_right: "<<t_right<<"\t t_left: "<<t_left<<endl;
        motorposition[0][i] = knee_angle(t_right);
        motorposition[1][i] = pretension(t_right);
        motorposition[2][i] = hip_angle(t_right);
        motorposition[3][i] = knee_angle(t_left);
        motorposition[4][i] = pretension(t_left);
        motorposition[5][i] = hip_angle(t_left);
    }
    
    motorposition_int = conversions(motorposition);
    
    return motorposition_int;
}

float hip_angle(float t)
{
    float y = a_Hip_Angle[1-1]+a_Hip_Angle[2-1]*cos(1*omega*t)+a_Hip_Angle[3-1]*sin(1*omega*t)+a_Hip_Angle[4-1]*cos(2*omega*t)+a_Hip_Angle[5-1]*sin(2*omega*t)+a_Hip_Angle[6-1]*cos(3*omega*t)+a_Hip_Angle[7-1]*sin(3*omega*t)+a_Hip_Angle[8-1]*cos(4*omega*t)+a_Hip_Angle[9-1]*sin(4*omega*t)+a_Hip_Angle[10-1]*cos(5*omega*t)+a_Hip_Angle[11-1]*sin(5*omega*t);
    return grad_to_rad(y);
}

float knee_angle(float t)
{
    float y = a_Knee_Angle[1-1]+a_Knee_Angle[2-1]*cos(1*omega*t)+a_Knee_Angle[3-1]*sin(1*omega*t)+a_Knee_Angle[4-1]*cos(2*omega*t)+a_Knee_Angle[5-1]*sin(2*omega*t)+a_Knee_Angle[6-1]*cos(3*omega*t)+a_Knee_Angle[7-1]*sin(3*omega*t)+a_Knee_Angle[8-1]*cos(4*omega*t)+a_Knee_Angle[9-1]*sin(4*omega*t)+a_Knee_Angle[10-1]*cos(5*omega*t)+a_Knee_Angle[11-1]*sin(5*omega*t);
    return grad_to_rad(y);   
}

float pretension(float t)
{
    float y = 0.55; // 1. [rad]  // Marc's comment : Maybe has to be changed !!!!!!!!!!!!!!!!
    return y;
}

float grad_to_rad(float angle)
{
    return angle*pi/180.;
}

vector<vector<int>> conversions(vector<vector<float>> motor_position)
{
    // iterate over 2D motor vector
    vector<vector<int>> motor_position_int(motors);
    for(int i=0;i<motors;i++)
    {
        motor_position_int[i].resize(next_motor_values);				
    }
    for(unsigned int i=0; i < motors;i++)
    {
        for(unsigned int j=0; j < next_motor_values;j++)
        {
            motor_position_int[i][j] = map(motor_position[i][j]);
        }
    }
    
    return motor_position_int;
}

int map(float x, float in_min, float in_max, float out_min, float out_max) // mapping function , default values declared in header
{
	return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}

#endif
