#include "Commands_Datenpacket.h"
#include <vector>
#include <cstring>


using namespace std;





Commands_Datenpacket::Commands_Datenpacket(unsigned long zeit)
{

	//Anzahl werte f�r einen motor in einem packet
	this->anzahl_motorenwerte = 1000; 
	//Gibt die Anzahl floats im Struckt an.
	this->nummerOfFloats = 6 * anzahl_motorenwerte + 100;

	//Die n�chsten motorenpositionen
	//Erste nummer ist f�r welchen motor
	//0 rechts hebel, 1 rechts rolle, 2 rechts h�fte, 3 links hebel, 4 links rolle, 5 links h�ft
	//Die zweite nummer ist die Position (zeitlich)
	this->motorposition.resize(6);
	for(int i =0;i<6;++i)
	{
		this->motorposition[i].resize(anzahl_motorenwerte);
	}


	//Gibt an wievile flagbytes es gibt
	this->anzahl_flagbytes = 12;

	//Flag bytes
	this->flags = std::vector<unsigned char>(anzahl_flagbytes);

	//Time in miliseconds of the datapacket

	time = zeit;
}

Commands_Datenpacket::Commands_Datenpacket()
{
	//Anzahl werte f�r einen motor in einem packet
	this->anzahl_motorenwerte = 1000; 

	//Gibt die Anzahl floats im Struckt an.
	this->nummerOfFloats = 6 * anzahl_motorenwerte + 100;

	//Die n�chsten motorenpositionen
	//Erste nummer ist f�r welchen motor
	//0 rechts hebel, 1 rechts rolle, 2 rechts h�fte, 3 links hebel, 4 links rolle, 5 links h�ft
	//Die zweite nummer ist die Position (zeitlich)
	this->motorposition.resize(6);
	for(int i =0;i<6;++i)
	{
		this->motorposition[i].resize(anzahl_motorenwerte);
	}


	//Gibt an wievile flagbytes es gibt
	this->anzahl_flagbytes = 12;

	//Flag bytes
	this->flags = std::vector<unsigned char>(anzahl_flagbytes);

	//Time in miliseconds of the datapacket

	time = 0;
}


unsigned long Commands_Datenpacket::getTime(){
	return time;
}


void Commands_Datenpacket::setTime(long ti){
	this->time = ti;

}


std::vector<unsigned char> Commands_Datenpacket::convertToByte()
{
	std::vector<unsigned char> packet(nummerOfFloats * 4);
	std::vector<float> reihe(nummerOfFloats);
	//std::vector<unsigned char> zwi(4);
	int position = 0;

	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < anzahl_motorenwerte; j++)
		{
			reihe[position] = (float)motorposition[i][ j];
			position++;
		}
	}

	for (int i = 0; i < anzahl_flagbytes; i++)
	{
		reihe[position] = flags[i];
		position++;
	}




	for (int i = 0; i < nummerOfFloats; i++)
	{
		float f = reihe[i];
		char zwi[sizeof(float)];

		memcpy(zwi, &f, (size_t) sizeof(float));

		//zwi = BitConverter.GetBytes(reihe[i]);
		packet[4 * i] = zwi[0];
		packet[4 * i + 1] = zwi[1];
		packet[4 * i + 2] = zwi[2];
		packet[4 * i + 3] = zwi[3];
	}




	std::vector<unsigned char> msg = createHeader(packet,time,3);

	return msg;



}


void Commands_Datenpacket::convertFromByte(std::vector<unsigned char> msg)
{
	std::vector<unsigned char> packet = detachHeader(msg);
	this->time = extractTime(msg);


	std::vector<float> reihe (nummerOfFloats);
	int position = 0;

	for (int i = 0; i < 6 * anzahl_motorenwerte; i++)
	{
		float f;
		unsigned char b[] = { packet[4 * i ], packet[4 * i + 1], packet[4 * i + 2], packet[4 * i+3] };
		memcpy(&f, &b, sizeof(f));

		reihe[i] = f;
	}

	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < anzahl_motorenwerte; j++)
		{
			motorposition[i][ j] = (int)reihe[position];
			position++;
		}
	}

	for (int i = 0; i < anzahl_flagbytes; i++)
	{
		flags[i] = (unsigned char)reihe[position];
		position++;

	}



}

int Commands_Datenpacket::getAnzahlBytes()
{
	return (6 * anzahl_motorenwerte + 100) * 4;
}
unsigned char Commands_Datenpacket::getID()
{

	return 3;
}
