#include "Intercom_datenpacket.h"
#include <vector>
#include <cstring>

using namespace std;






unsigned long Intercom_datenpacket::getTime(){
	return time;
}

Intercom_datenpacket::Intercom_datenpacket(long anzahldaten, unsigned long zeit)
{

	this->anzahl_daten= anzahldaten;
	this->anzahl_bytes = anzahldaten * 5;
	std::vector<unsigned char> com1(anzahldaten);
	std::vector<float> data1(anzahldaten);
	this->time = zeit;
	this->com = com1;
	this->data = data1;

}
Intercom_datenpacket::Intercom_datenpacket()
{
	std::vector<unsigned char> com1(20);
	std::vector<float> data1(20);
	this->com = com1;
	this->data = data1;
	this->anzahl_daten = 20;
	this->anzahl_bytes = 100;
}



 long Intercom_datenpacket::getAnzahlBytes()
{
	return anzahl_bytes;
}

 unsigned char Intercom_datenpacket::getID()
{
	return 4;
}

 std::vector<unsigned char> Intercom_datenpacket::convertToByte()
{


	std::vector<unsigned char> msg(anzahl_bytes);
	std::vector<unsigned char>zwi(4);


	for (int i = 0; i < anzahl_daten; i++)
	{
		

		float f = data[i];
		char a[sizeof(float)];

		memcpy(a, &f, (size_t) sizeof(float));
		/*
		//Converts time to the array
		float z = data[i];
		float *a = &z;
		zwi[0] = *a;
		zwi[1] = *(a + 1);
		zwi[2] = *(a + 2);
		zwi[3] = *(a + 3);
		*/

		msg[5 * i] = com[i];
		msg[5 * i + 1] = a[3];
		msg[5 * i + 2] = a[2];
		msg[5 * i + 3] = a[1];
		msg[5 * i + 4] = a[0];
	}

	std::vector<unsigned char> packet = createHeader(msg,time,4);
	return packet;




}

  void Intercom_datenpacket::setTime(long ti){
	 this->time = ti;

 }


 void Intercom_datenpacket::convertFromByte(std::vector<unsigned char> msg)
 {
	 std::vector<unsigned char> packet = detachHeader(msg);
	this->time = extractTime(msg);

	 anzahl_daten =(long)(packet.size() / 5);
	 anzahl_bytes = packet.size();

	 for (int i = 0; i < anzahl_daten; i++)
	 {
		 com[i] = packet[5 * i];

		 //converts byte array to float
		 float f;
		 unsigned char b[] = { packet[5 * i + 4], packet[5 * i + 3], packet[5 * i + 2], packet[5 * i + 1] };
		 memcpy(&f, &b, sizeof(f));

		 data[i] = f;
	 }

 }


