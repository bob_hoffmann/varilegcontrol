#include "Packet_Reciever.h"
#include "IDatenPacket.h"
#include "Intercom_datenpacket.h"
#include "Output_Datenpacket.h"
#include "Sensor_Datenpacket.h"
#include "Commands_Datenpacket.h"
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <zmq.hpp>


// Bob's includes
#include <math.h>
#include "control_f.h"

//global variable
long virtual_time = 0;

using namespace std;



zmq::context_t *context;
zmq::socket_t *socket;
Packet_Reciever rec;





// function prototypes


std::vector<unsigned char>  rec_msg();
void send(std::vector<unsigned char>  msg);
void init_zmq();

/*
 * Code fuer BA und Controlling und so hier in die Event getriggerten Funktionen schreiben
*/



void OnInterRec(Intercom_datenpacket icd){


}

void OnComRec(Commands_Datenpacket cod){


}


void OnOutRec(Output_Datenpacket odp){


}


void OnSensRec(Sensor_Datenpacket sd){				// Marc's comment: GENERAL QUESTION: DON'T YOU NEED ANY GLOBAL VARIABLES?
	//end time measurement							
    end_time= std::chrono::steady_clock::now();
	
    long duration = chrono::duration_cast<chrono::microseconds>(end_time - begin_time).count(); // [microseconds]
    //start time measurement
	begin_time = std::chrono::steady_clock::now();

    // Bob's time
    virtual_time = virtual_time + duration;
   
    vector<vector<float>> motorposition_int(6); //6 motors
    for(int i=0;i<6;i++)
    {
        motorposition_int[i].resize(next_motor_values);				
    }
    
    motorposition_int = calc_trajectories(virtual_time,next_motor_values,update_t_slave);

    // command package
	Commands_Datenpacket *cod=new Commands_Datenpacket(3);
    cod->motorposition=motorpositions_int;

	send(cod->convertToByte());

}





int main(int argc, const char* argv[])
{
	init_zmq();
	//initialize first time start timer
	begin_time = std::chrono::steady_clock::now();
    
    control_init();

	while(true){
		rec_msg();					
	}
	

}







void DecodeRecMsg(std::vector<unsigned char> msg){

	if(msg[0]==1){
		OnSensRec(rec.reciveSensor(msg));
	
	}
	if(msg[0]==2){
		OnOutRec(rec.reciveOutput(msg));

	}

	if(msg[0]==3){
		OnComRec(rec.reciveComand(msg));

	}
	if(msg[0]==4){
		OnInterRec(rec.reciveIntercom(msg));
	
	}

}


std::vector<unsigned char>  rec_msg(){

	zmq::message_t request;
	socket->recv(&request);
	vector<unsigned char> msg(request.size());
	memcpy((void *)msg.data(), request.data(), request.size());
	return msg;

}


void send(std::vector<unsigned char>  msg){

	 zmq::message_t reply( msg.size() );
	 memcpy((void *)reply.data(), msg.data(), msg.size());
	 socket->send(reply);
	

}

 void init_zmq(){

	 context = new zmq::context_t(1);
	 socket = new zmq::socket_t(*context, ZMQ_PAIR);
	 socket->bind("tcp://*:5555");

 }



