#include <stdint.h>
#include "data_interface.h"
#include "position_controller.h"
#include "defines.h"
#include <stdio.h>

int main()
{
     Master_Data master_data = {
		199,
		{13,9,2,1,88},
		{0,0,0,0,0},
		5,
		0,
		6,
		0,
		7,
		8
	};
    int16_t rpm = 0;
    int16_t ticks = 0;
    uint16_t softpot = 2348;
    uint16_t soft_pot_2 = 0;
    int i;
    for(i=0;i<10000; i++)
    {
        uint16_t pwm = position_controll_mcu1(rpm,ticks,softpot,soft_pot_2,&master_data);
        printf("pwm value: %d \n",pwm);
    }
    return 0;
}
