



/*	===============================================
 * 	Set PWM speed according to the following rules:
 * 	820  --> max reverse speed (CCW)
 * 	4096 --> zero speed (motor forces to stand still)
 * 	7372 --> max forward speed (CW)
 * 	===============================================
 */

//Master_Data struct

// Bob's global variables
#include "own_libs/defines.h"
// 

#define pi 3.14159265359

////// Safety Parameters //////
// Joint
// Virtual Stop
const float virtual_min_mcu_1 = -pi/2+0.1;
const float virtual_min_mcu_2 = -pi/2+0.1;
const float virtual_max_mcu_1 = pi/2-0.1;
const float virtual_max_mcu_2 = pi/2-0.1;
// Final Stop
const float min_mcu_1 = -pi/2;
const float min_mcu_2 = -pi/2;
const float max_mcu_1 = pi/2;
const float max_mcu_2 = pi/2;


////// PID Controller //////
// Errors
float previous_error = 0;
float integral = 0;
float error = 0;
float derivative = 0;
float PID_output = 0;
int motor_position_int_old = -1;
// Parameters
float KP;
float KI;
float KD;

////// Data transfer //////
// new_packet
uint8_t new_packet = 0;
uint8_t counter_old = 0;
uint8_t iterator = 0;

// mapping parameters master slave
const float out_min_ms = -pi;
const float out_max_ms= pi;
const float in_min_ms = 0;
const float in_max_ms = 65536;

// motor constants
const float max_motor_rpm = 3500; // [rpm]
const float max_motor_rad_s = 3500/60*2*pi; // [rad/s]
const float conversion_coeff = 60/2*pi; // [RPM 1/(rad/s) ] v_rpm = conversion_coeff * v_rad_s

// mapping parameters pwm
const float out_min_pwm = 820;
const float out_max_pwm= 7372;
const float in_min_pwm = -3500/60*2*pi;
const float in_max_pwm = 3500/60*2*pi;

// constants related to data transfer
const int POS_FREQUENCY = POS_CONTROLL_FREQ;

/*
const int POS_VALUES = POS_CONTROLL_VAL_COUNT;
*/

/* /Testing/ */
const int POS_VALUES = 63;
int slow_loop = 100;
int slow_iterator = 0;
// 10°*sin() + 10°
int POS[] = {36409,36772,37132,37485,37827,38154,38465,38754,39021,39261,39473,39654,39802,39917,39997,40041,40048,40019,39955,39854,39720,39552,39353,39124,38868,38588,38286,37965,37629,37280,36923,36560,36196,35835,35478,35132,34798,34480,34181,33905,33653,33430,33236,33073,32944,32850,32791,32768,32782,32832,32918,33038,33192,33379,33595,33840,34111,34404,34717,35048,35392,35746,36106};
/* /Testing/ */


//pos controller for MCU 1
uint16_t position_controll_mcu1(int16_t mcu1_rpm, int16_t mcu1_tick_offset, uint16_t soft_pot_1, uint16_t soft_pot_2, Master_Data *data)
{

	/*
    // PID Parameters
	float KP = data->MCU1_PID_P;
	float KI = data->MCU1_PID_I;
	float KD = data->MCU1_PID_D;
	*/	

    /* Testing */
    // PID Parameters
	float KP = 0.1;
	float KI = 0;
	float KD = 0;
    /* Testing */

    /* /Testing/ */
    data->COUNTER = 0;
    
    slow_loop--;
    if(slow_loop <= 0)
    {
        slow_loop = 100;
        slow_iterator++;
        if(slow_iterator>POS_VALUES)
        {
            slow_iterator = 0;
        }
        data->MCU1_POS[0] = POS[slow_iterator];
        data->MCU1_POS[1] = POS[slow_iterator];
        data->MCU1_POS[2] = POS[slow_iterator];
        data->MCU1_POS[3] = POS[slow_iterator];
    }
    else
    {
        iterator = 0;
    }
    /* /Testing/ */


	// Time
	float dt = 1/POS_FREQUENCY;

	// check if new packet
	if(counter_old != data->COUNTER)
	{
		iterator = 0;
	}
	else
	{
		if(iterator+2<POS_VALUES-1) // check bounds to avoid out of range array access during vcnt calculation
		{
			iterator++;
		}
		else
		{
			iterator = POS_VALUES-2;
		}
	}
	// end check if new packet

	uint16_t new_pwm_speed = 0;
	float new_motor_speed = 0;

	// access motor value
    int motor_position_int = (*data).MCU1_POS[iterator];
    int motor_position_int_1 = (*data).MCU1_POS[iterator+1];
	// inverse mapping as "map" in control_functions.cpp
	float motor_position = 	(motor_position_int - in_min_ms) * (out_max_ms - out_min_ms) / (in_max_ms - in_min_ms) + out_min_ms; // [rad]
    float motor_position_1 = 	(motor_position_int_1 - in_min_ms) * (out_max_ms - out_min_ms) / (in_max_ms - in_min_ms) + out_min_ms; // [rad]


    // Joint angle
    
    float joint_angle_1 = SOFT_POT_TO_RAD(soft_pot_1);
    
    ////// Safety 2. ///////

    // Check joint angles
    if(joint_angle_1<virtual_min_mcu_1)
    {
        if(motor_position<virtual_min_mcu_1)
        {
            motor_position = virtual_min_mcu_1;
            motor_position_1 = virtual_min_mcu_1;
        }
    }
    if(joint_angle_1>virtual_max_mcu_1)
    {
        if(motor_position>virtual_max_mcu_1)
        {
            motor_position = virtual_max_mcu_1;
            motor_position_1 = virtual_max_mcu_1;
        }
    }

	////// PID Controller ///////

	// calculate proportional error
    error = motor_position - joint_angle_1;

	// calculate derivative error
    derivative = (error-previous_error)/dt;
    /* // for hip and pretension:
     * if(motor_position_int_old != -1)
     * {
     * 		derivative = (motor_position_int -motor_position_int_old)/dt - mcu1_rpm;
     * }
     * else
     * {
     * 		derivative = 0;
     * }
     */

	// calculate integrale error
    integral = integral + error*dt;

	// Sum PID errors
    PID_output = KP*error + KI*integral + KD*derivative;

	// end PID Controller
	
	// velocity feedforward	
    float vcnt = (motor_position_1-motor_position)/dt;

	// sum PID and velocity feedforward
    new_motor_speed = PID_output + vcnt;
	

	// tranformation rad/s to pwm
	new_pwm_speed = (int)((new_motor_speed - in_min_pwm) * (out_max_pwm - out_min_pwm) / (in_max_pwm - in_min_pwm) + out_min_pwm);

	// store old values
	// Packet counter
	counter_old = data->COUNTER;
	// PID controller
	previous_error = error;
	// Reference position
	motor_position_int_old = motor_position_int;

	// a posteriori anti-windup
	if(new_motor_speed > max_motor_rad_s)
	{
		integral = integral - error*dt;
	}


    ////// Safety 2. ///////

    // Check joint angles
    if(joint_angle_1<min_mcu_1)
    {
        new_pwm_speed = 4096;
    }
    if(joint_angle_1>max_mcu_1)
    {
        new_pwm_speed = 4096;
    }
    // Check motor speed
    if(new_pwm_speed>7372)
    {
        new_pwm_speed = 7372;
    }
    if(new_pwm_speed<820)
    {
        new_pwm_speed = 820;
    }
    

	return new_pwm_speed;
}


//pos controller for MCU 2
uint16_t position_controll_mcu2(int16_t mcu2_rpm, int16_t mcu2_tick_offset, uint16_t soft_pot_3, Master_Data *data)
{
	uint8_t new_pwm_speed = 0;



	return new_pwm_speed;
}

